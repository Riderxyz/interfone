/* enum EnviromentValues {
  localUserId,
  selectedChamadaId,
  prodFirebase,
  devFirebase,
} */
enum ConfigViewListTypes {
  editAccount,
  qrCode,
  automation,
  editProfile,
  exitApp,
}
enum AppCurrentStatus {
  active,
  inactive
}

enum CallStatus {
  accepted,
  denied,
  lost,
  nullStatus
}

