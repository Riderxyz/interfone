import 'package:evox/app/modules/home/views/home_view.dart';
import 'package:evox/app/modules/login/views/login_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:uuid/uuid.dart';
import 'data.service.dart';

class AuthService extends GetxService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final DataService dataService = Get.find<DataService>();

  RxBool isLoading = false.obs;
  Rxn<User> firebaseUser = Rxn<User>();

  Future<AuthService> init() async {
    _auth.authStateChanges().listen((User? user) async {
      if (user == null) {
        print('No user is logged in.');
        Get.off(() => LoginView());
      } else {
        print('User is logged in: ${user.uid}');
        await _initializeUser(user);
        Get.off(() => HomeView());
      }
    });
    return this;
  }

  Future<void> loginWithGoogle() async {
    isLoading.value = true;
    try {
      final GoogleSignInAccount? googleUser = await _googleSignIn.signIn();
      if (googleUser == null) {
        isLoading.value = false;
        return; // Login canceled by the user.
      }

      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;

      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      final UserCredential userCredential =
          await _auth.signInWithCredential(credential);

      await _initializeUser(userCredential.user!);
    } catch (e) {
      _showErrorSnackbar("Login Error", e.toString());
    } finally {
      isLoading.value = false;
    }
  }

  Future<void> _initializeUser(User user) async {
    dataService.currentUsuario.id = user.uid;

    final userDoc = await dataService.userData.get();

    if (userDoc.exists) {
      // Existing user: load their data
      dataService.currentUsuario = userDoc.data()!;
    } else {
      // New user: create their record
      await _handleNewUser(user);
    }
  }

  Future<void> _handleNewUser(User user) async {
    dataService.currentUsuario
      ..id = user.uid
      ..email = user.email
      ..nome = user.displayName
      ..photo = user.photoURL ?? ''
      ..tokenChamada = const Uuid().v4()
      ..dataCadastro = DateTime.now().millisecondsSinceEpoch
      ..dataAtualizacao = DateTime.now().millisecondsSinceEpoch;

    await dataService.saveUserData();
  }

  Future<void> signOut() async {
    try {
      await _googleSignIn.signOut();
      await _auth.signOut();
    } catch (e) {
      rethrow;
    }
  }

  void _showErrorSnackbar(String title, String message) {
    Get.snackbar(
      title,
      message,
      snackPosition: SnackPosition.TOP,
      backgroundColor: Colors.red,
      colorText: Colors.white,
    );
  }
}
