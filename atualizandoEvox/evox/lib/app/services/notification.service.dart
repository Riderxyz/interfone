import 'dart:async';
import 'dart:io';

import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:awesome_notifications_fcm/awesome_notifications_fcm.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:evox/app/modules/chamada/backgroundCall/callKeepConfig.dart';
import 'package:evox/app/modules/chamada/backgroundCall/callKeepEventSwitcher.dart';
import 'package:evox/app/models/pushNotificationData.model.dart';
import 'package:evox/app/services/data.service.dart';
import 'package:flutter/material.dart';
import 'package:android_id/android_id.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:get/get.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class NotificationService extends GetxService {
  final FirebaseFirestore firestore = FirebaseFirestore.instance;
  //final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
  final _androidIdPlugin = const AndroidId();

  var identifier = '';
  String userPhoneFCMtoken = '';
  late Map<String, dynamic> pushRetrivedData;
  Future<NotificationService> init() async {
    await getAndroidId();
    await initializeNotifications();
    return this;
  }

  getAndroidId() async {
    try {
      if (Platform.isAndroid) {
        _androidIdPlugin.getId().then((androidData) {
          if (androidData != null) {
            identifier = androidData; //UUID for Android
          }
        }).catchError((onError) {
          print(onError);
        });
      }
    } catch (e) {
      print('Deu tudo errado ao pegar o UUID do celular');
      print(e);
    }
  }

  saveTokenToFirestore(String token, String userUID) {
    print(identifier);
    print('token de mensagem =  ' + token.toString());
    firestore
        .collection('usuarios')
        .doc(userUID)
        .collection('notificationToken')
        .doc(identifier)
        .set({
      'platorm': Platform.isAndroid ? 'Android' : 'IOS',
      'criadoEm': DateTime.now().millisecondsSinceEpoch,
      'token': token
    });
  }

  Future<void> initializeNotifications() async {

 AwesomeNotifications().isNotificationAllowed().then((isAllowed) {
      if (!isAllowed) {
        AwesomeNotifications().requestPermissionToSendNotifications();
      }
    });
    await AwesomeNotifications().initialize(
        null, //'resource://drawable/res_app_icon',//
        [
          NotificationChannel(
              channelKey: 'alerts',
              channelName: 'Alerts',
              channelDescription: 'Notification tests as alerts',
              // soundSource: 'resource://raw/ringtone',
              locked: true,
              //onlyAlertOnce: true,
              //   groupAlertBehavior: GroupAlertBehavior.Children,
              importance: NotificationImportance.High,
              //defaultPrivacy: NotificationPrivacy.Private,
              defaultColor: Colors.deepPurple,
              ledColor: Colors.deepPurple),
          NotificationChannel(
              channelKey: 'ligacao',
              channelName: 'ligacoes',
              defaultPrivacy: NotificationPrivacy.Private,
              channelDescription: 'Ligações para este canal',
              defaultColor: Colors.black,
              channelShowBadge: true,
              importance: NotificationImportance.Max,
              ledColor: Colors.red,
              playSound: true,
          
              //soundSource: 'assets/sound/phoneCall.mp3',
              enableVibration: true),
        ],
        debug: true);


    await AwesomeNotificationsFcm().initialize(
      onFcmTokenHandle: saveFCMToken,
      onFcmSilentDataHandle: onFcmSilentDataHandle,
    );
  }


@pragma("vm:entry-point")
static Future<void> onFcmSilentDataHandle(FcmSilentData silentData) async {
  print('Silent data received: $silentData');
  // Add your handling logic here
}

/// Use this method to detect when a new fcm token is received
  @pragma("vm:entry-point")
  static Future<void> saveFCMToken(String token) async {
    print('Token FCM: $token');
    print('FCM Token received: $token');
; // Returns true if the NotificationService is registered in GetX
    // Access the GetX instance of NotificationService
    NotificationService notificationSrv = Get.isRegistered<NotificationService>() ? Get.find<NotificationService>() : Get.put(NotificationService());
      notificationSrv.userPhoneFCMtoken = token;
      
  }


}