import 'package:get/get.dart';

class UserProvider extends GetConnect {
Future<Response> getUserAdress(String cep) => get('https://viacep.com.br/ws/$cep/json/');
}