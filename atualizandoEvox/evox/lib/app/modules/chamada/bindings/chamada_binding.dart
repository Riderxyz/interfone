import 'package:get/get.dart';

import '../controllers/chamada_controller.dart';

class ChamadaBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ChamadaController>(
      () => ChamadaController(),
    );
  }
}
