import 'package:evox/app/modules/chamada/backgroundCall/backgroundCallAcceptFunction.dart';
import 'package:evox/app/modules/chamada/backgroundCall/backgroundCallDeclineFunction.dart';
import 'package:evox/app/modules/chamada/backgroundCall/backgroundCallHangUp.dart';
import 'package:evox/app/modules/chamada/backgroundCall/backgroundCallTimeOutFunction.dart';
import 'package:evox/app/models/pushNotificationData.model.dart';
import 'package:flutter_callkit_incoming/entities/call_event.dart';

onCallKeepEventReceived(CallEvent? event, PushNotificationData pushData) {
  switch (event!.event) {
    case Event.actionCallAccept:
      backgroundFunctionOnAccept(pushData);
      break;
    case Event.actionCallDecline:
      backgroundFunctionOnDecline(pushData);
      break;
    case Event.actionCallEnded:
      backgroundFunctionOnHangUp(pushData);
      break;
    case Event.actionCallTimeout:
      backgroundFunctionOnTimeOut(pushData);
      break;
    default:
      break;
  }
}
