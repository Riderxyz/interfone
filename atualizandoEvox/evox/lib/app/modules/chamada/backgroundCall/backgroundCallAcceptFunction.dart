/* import 'dart:developer';
 */
import 'dart:ui';

import 'package:evox/app/models/pushNotificationData.model.dart';

///
/// A chamada foi atendida pelo usuario;
Future<void> backgroundFunctionOnAccept(PushNotificationData pushData) async {
  print(pushData);
  IsolateNameServer.lookupPortByName('portForAgoraIoCall')
      ?.send('HERE ME CALLIN INTO YOU, EEEOOONNSSS!!!');
  /* Get.to(() => ChamadaView(),
      transition: Transition.upToDown,
      duration: Duration(milliseconds: 500),
      curve: Curves.easeIn);   */
  return;
}
