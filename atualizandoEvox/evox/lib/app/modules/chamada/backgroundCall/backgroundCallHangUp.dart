// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:evox/app/models/chamada.model.dart';
import 'package:evox/app/models/pushNotificationData.model.dart';
import 'package:evox/app/modules/home/views/home_view.dart';
import 'package:get/get.dart';
import 'package:flutter/foundation.dart';

///
/// A chamada foi atendida e foi encerrada pelo usuario;
Future<void> backgroundFunctionOnHangUp(PushNotificationData pushData) async {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  Chamada userCallTempData = Chamada(
      data: 0,
      endTime: 0,
      startTime: 0,
      img: '',
      isChamadaFinalizada: false,
      nome: '',
      tempoChamada: 0,
      localizacaoLat: 0,
      localizacaoLong: 0);
  var valueCall = await _firestore
      .collection('usuarios')
      .doc(pushData.proprietarioId)
      .collection('chamadaAtiva')
      .doc('chamadaAtiva')
      .get();
  userCallTempData = Chamada.fromJson(valueCall.data() as Map<String, dynamic>);
  if (kDebugMode) {
    print('O nome do maluco q ta ligando é');
  }
  if (kDebugMode) {
    print(userCallTempData.nome);
  }

  userCallTempData.endTime = DateTime.now().millisecondsSinceEpoch;
  int? startTime = userCallTempData.startTime;
  int? endTime = userCallTempData.endTime;
  if (userCallTempData.startTime != null) {
    userCallTempData.tempoChamada = endTime! - startTime!;
  }
  userCallTempData.isChamadaFinalizada = true;
  _firestore
      .collection('usuarios')
      .doc(pushData.proprietarioId)
      .collection('chamadaAtiva')
      .doc('chamadaAtiva')
      .update({'isChamadaFinalizada': true});
  _firestore
      .collection('usuarios')
      .doc(pushData.proprietarioId)
      .collection('historicoChamadas')
      .add({
    'nome': userCallTempData.nome,
    'img': userCallTempData.img,
    'data': userCallTempData.data,
    'tempoChamada': userCallTempData.tempoChamada,
    'startTime': userCallTempData.startTime,
    'endTime': userCallTempData.endTime,
    'isChamadaFinalizada': true,
  });
  if (Get.context != null) {
    Get.off(() => HomeView());
  }
}
