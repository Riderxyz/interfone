import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/chamada_controller.dart';

class ChamadaView extends GetView<ChamadaController> {
  const ChamadaView({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ChamadaView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'ChamadaView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
