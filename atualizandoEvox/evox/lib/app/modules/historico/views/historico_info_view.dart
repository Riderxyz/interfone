import 'package:evox/app/modules/historico/controllers/historico_controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class HistoricoInfoView extends GetView<HistoricoController> {
  const HistoricoInfoView({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('HistoricoInfoView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'HistoricoInfoView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
