import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/historico_controller.dart';

class HistoricoView extends GetView<HistoricoController> {
  const HistoricoView({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('HistoricoView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'HistoricoView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
