import 'package:flutter/material.dart';

import 'package:get/get.dart';

class CadastroView extends GetView {
  const CadastroView({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('CadastroView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'CadastroView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
