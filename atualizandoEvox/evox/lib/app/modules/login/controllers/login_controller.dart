import 'package:get/get.dart';
import 'package:evox/app/services/auth.service.dart';

class LoginController extends GetxController {
  final AuthService authSrv = Get.find<AuthService>();


  @override
  void onReady() {
    super.onReady();
  }

  Future<void> loginWithGoogle() async {
    authSrv.isLoading.value = true;
    try {
      await authSrv.loginWithGoogle();
    } catch (error) {
      Get.snackbar(
        "Erro ao fazer login",
        "Ocorreu um problema ao tentar fazer login com o Google. Tente novamente.",
        snackPosition: SnackPosition.TOP,
      );
    } finally {
      authSrv.isLoading.value = false;
    }
  }
}
