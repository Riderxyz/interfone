import 'package:evox/app/models/chamada.model.dart';
import 'package:evox/app/services/auth.service.dart';
import 'package:evox/app/services/data.service.dart';
import 'package:evox/app/services/notification.service.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class HomeController extends GetxController {
  final AuthService authSrv = Get.find<AuthService>();
  final DataService dataSrv = Get.find<DataService>();
  final NotificationService notificationSrv = Get.find<NotificationService>();
  var list = [];
  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
    print(dataSrv.currentUsuario.nome);
    print(dataSrv.currentUsuario);
    /*   Future.delayed(const Duration(seconds: 60)).then((value) {
      authSrv.signOut();
    }); */
  }

  @override
  void onReady() {
    super.onReady();
  }

  set selectedChamadaForDetail(Chamada chamada) {
    dataSrv.selectedChamadaForDetail = chamada;
  }

  String formatTimeInChamadas(int timestamp) {
    String time = '';
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(timestamp);
    time = DateFormat('dd/MM/yyyy HH:mm').format(dateTime);
    return time;
  }

  @override
  void onClose() {
    super.onClose();
  }

  void increment() => count.value++;

  test() {
    print('Testando');
  }
}
