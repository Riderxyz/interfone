import 'package:evox/app/components/sidebarMenu/side_bar_menu_view.dart';
import 'package:evox/app/models/chamada.model.dart';
import 'package:evox/app/modules/historico/views/historico_info_view.dart';
import 'package:evox/app/modules/qrCode/qr_code_view.dart';


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/home_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:animate_do/animate_do.dart';
import 'package:animations/animations.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
class HomeView extends GetView<HomeController> {
  HomeView({super.key});
  @override
  HomeController controller = Get.isRegistered<HomeController>() ? Get.find<HomeController>() : Get.put(HomeController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Evoxcall',
            style: GoogleFonts.inter(
                color: Colors.black, fontWeight: FontWeight.bold)),
        backgroundColor: Colors.amber,
        centerTitle: true,
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      drawer: SideBarMenuView(),
      backgroundColor: const Color(0XFF1f1d2b),
      body: BounceInUp(
        from: 200,
        duration: const Duration(milliseconds: 2000),
        child: Padding(
          padding:
              const EdgeInsets.only(left: 20, right: 20, bottom: 0, top: 40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: (() => controller.test()),
                child: Text(
                  'Boas vindas ${controller.dataSrv.currentUsuario.nome}',
                  textAlign: TextAlign.start,
                  style: GoogleFonts.inter(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: const Color(0xFFFFC61A),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                'Últimas chamadas recebidas',
                textAlign: TextAlign.start,
                style: GoogleFonts.inter(
                  fontSize: 16,
                  color: Colors.white,
                ),
              ),
              const SizedBox(height: 30),
              StreamBuilder(
                stream: FirebaseFirestore.instance
                    .collection('usuarios')
                    .doc(controller.dataSrv.currentUsuario.id)
                    .collection('historicoChamadas')
                    .orderBy('data', descending: false)
                    .limitToLast(3)
                    .snapshots(), //FirebaseFirestore.instance.collection('usuarios').snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.hasError) {
                    return Center(
                        child: Text('Achei um erro aqui → ${snapshot.error}'));
                  }
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Center(child: CircularProgressIndicator());
                  }
                  controller.list = snapshot.data!.docs;
                  var retornoTest = controller.list.isNotEmpty;
                  return retornoTest
                      ? ListView.builder(
                          itemCount: controller.list.length,
                          shrinkWrap: true,
                          reverse: true,
                          itemBuilder: (BuildContext context, int i) {
                            Chamada chamadaAtual = Chamada.fromJson(
                                snapshot.data!.docs[i].data()
                                    as Map<String, dynamic>);
                            return Padding(
                              padding: const EdgeInsets.only(bottom: 20),
                              child: Container(
                                decoration: const BoxDecoration(
                                  color: Color(0xFF252836),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(20),
                                  ),
                                ),
                                child: OpenContainer(
                                  middleColor: Colors.amber,
                                  openElevation: 10,
                                  closedElevation: 15,
                                  closedShape: const RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                  ),
                                  closedColor: const Color(0xFF252836),
                                  transitionType: ContainerTransitionType.fade,
                                  transitionDuration:
                                      const Duration(milliseconds: 900),
                                  openBuilder: (context, _) =>
                                      const HistoricoInfoView(),
                                  closedBuilder: (context, openContainer) =>
                                      SizedBox(
                                    height: 85,
                                    child: Center(
                                      child: ListTile(
                                        onTap: (() {
                                          controller.selectedChamadaForDetail =
                                              chamadaAtual;
                                          openContainer();
                                        }),
                                        title: Text(
                                          chamadaAtual.nome,
                                          style: GoogleFonts.inter(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20,
                                              color: Colors.white),
                                        ),
                                        subtitle: Text(
                                          'Data da ligação: ${controller.formatTimeInChamadas(chamadaAtual.data)}',
                                          style: GoogleFonts.inter(
                                              fontSize: 15, color: Colors.grey),
                                        ),
                                        leading: SizedBox(
                                          height: 50,
                                          width: 50,
                                          child: Container(
                                            decoration: BoxDecoration(
                                                color: const Color(0xFF2F3142),
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                            child: const Center(
                                              child: FaIcon(
                                                FontAwesomeIcons.userLarge,
                                                color: Colors.white,
                                                size: 15,
                                              ),
                                            ),
                                          ),
                                        ),
                                        selected: false,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            );
                          })
                      : Center(
                          child: OpenContainer(
                            middleColor: Colors.amber,
                            openElevation: 10,
                            closedElevation: 15,
                            closedShape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                            ),
                            closedColor:
                                Colors.transparent, //Color(0xFF252836),
                            transitionType: ContainerTransitionType.fade,
                            transitionDuration:
                                const Duration(milliseconds: 900),
                            openBuilder: (context, _) => const QrCodeView(),
                            closedBuilder: (context, openContainer) =>
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10, right: 10, bottom: 10, top: 10),
                                  child: SizedBox(
                                    height: 80,
                                    width: MediaQuery.of(context).size.width - 80,
                                    child: ElevatedButton(
                                      style: ButtonStyle(
                                        backgroundColor:
                                            WidgetStateProperty.all<Color>(
                                                Colors.amber),
                                      ),
                                      onPressed: (() => openContainer()),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          const FaIcon(
                                            FontAwesomeIcons.qrcode,
                                            color: Color(0XFF1f1d2b),
                                          ),
                                          const SizedBox(
                                            width: 15,
                                          ),
                                          Expanded(
                                            child: Text(
                                                'Ainda sem chamadas? Clique aqui para baixar imprimir seu QRCode:',
                                                //   textAlign: TextAlign.right,
                                                style: GoogleFonts.inter(
                                                  color: const Color(0XFF1f1d2b),
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 15,
                                                )),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                          ),
                        );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }



}
