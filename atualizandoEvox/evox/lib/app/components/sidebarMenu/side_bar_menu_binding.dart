import 'package:get/get.dart';

import './side_bar_menu_controller.dart';

class SideBarMenuBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SideBarMenuController>(
      () => SideBarMenuController(),
    );
  }
}
