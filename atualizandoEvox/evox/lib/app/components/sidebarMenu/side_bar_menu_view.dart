// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:get/get.dart';

import './side_bar_menu_controller.dart';

class SideBarMenuView extends GetView<SideBarMenuController> {
  SideBarMenuView({super.key});
  @override
  SideBarMenuController controller = Get.put(SideBarMenuController());
  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: const Color(0xff252836),
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          SizedBox(
            //height: 90,
            width: double.maxFinite,
            child: DrawerHeader(
              decoration: const BoxDecoration(
                color: Colors.amber,
              ),
              padding: const EdgeInsets.all(15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 100,
                  ),
                  Text(
                    controller.dataSrv.currentUsuario.nome
                        .toString(),
                    textAlign: TextAlign.start,
                    style: const TextStyle(color: Colors.white, fontSize: 25),
                  ),
                ],
              ),
            ),
          ),
          ListView.builder(
            itemCount: controller.appPages.length,
            shrinkWrap: true,
            itemBuilder: ((vl, i) {
              return ListTile(
                title: Text(controller.appPages[i]['title'].toString()),
                leading: FaIcon(
                  controller.appPages[i]['icon'],
                  size: 20,
                ),
                enableFeedback: true,
                onTap: (() =>
                    controller.goTo(controller.appPages[i]['path'].toString())),
              );

              /*   Text(
                controller.appPages[i]['title'].toString(),
              ); */
            }),
          ),
        ],
      ),
    );
  }
}
