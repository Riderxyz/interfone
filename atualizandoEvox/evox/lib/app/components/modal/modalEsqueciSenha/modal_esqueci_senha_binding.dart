import 'package:get/get.dart';

import 'modal_esqueci_senha_controller.dart';

class ModalEsqueciSenhaBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ModalEsqueciSenhaController>(
      () => ModalEsqueciSenhaController(),
    );
  }
}
