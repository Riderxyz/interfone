import 'package:evox/app/services/auth.service.dart';
import 'package:evox/app/services/data.service.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';


class ModalEsqueciSenhaController extends GetxController {
  final GlobalKey<FormState> esqueciSenhaFormKey = GlobalKey<FormState>();
  final AuthService authSrv = Get.find<AuthService>();
  final DataService dataSrv = Get.find<DataService>();

  late TextEditingController emailController;
  //final Dio dio = Dio();
  //late DIO.Response _response;
  @override
  void onInit() {
    emailController = TextEditingController();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  String? validateEmail(String value) {
    String? retorno;
    if (!GetUtils.isEmail(value)) {
      retorno = 'Email inválido';
    }
    return retorno;
  }

  bool checkLogin() {
    final retorno = esqueciSenhaFormKey.currentState!.validate();
    return retorno;
  }

  void sendResetEMail() {
    if (checkLogin()) {
      esqueciSenhaFormKey.currentState!.save();
      print(emailController.value.text);

    //  authSrv.forgottenPassword(emailController.value.text);
    } else {
      return;
    }
  }

  Widget makeDismissible(context, Widget child) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => Navigator.of(context).pop(),
      child: GestureDetector(
        onTap: () {},
        child: child,
      ),
    );
  }

  @override
  void onClose() {}
}
