// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import 'modal_esqueci_senha_controller.dart';

class ModalEsqueciSenhaView extends GetView<ModalEsqueciSenhaController> {
/*   ModalEsqueciSenhaController controller =
      Get.put(ModalEsqueciSenhaController()); */
  @override
  Widget build(BuildContext context) {
    return controller.makeDismissible(
      context,
      DraggableScrollableSheet(
        initialChildSize: 0.7,
        minChildSize: 0.4,
        maxChildSize: 0.8,
        builder: (_, scrollController) => Container(
          color: Colors.transparent,
          child: Container(
            margin: const EdgeInsets.only(left: 20, right: 20),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(20),
              ),
            ),
            padding: const EdgeInsets.only(left: 16, right: 16),
            child: ListView(
              controller: scrollController,
              shrinkWrap: true,
              children: [
                Form(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  key: controller.esqueciSenhaFormKey,
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 16,
                      ),
                      const FaIcon(
                        FontAwesomeIcons.lock,
                        color: Colors.black,
                        size: 80,
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Title(
                        color: Colors.black,
                        child: const Text(
                          'Esqueceu a senha?',
                          style: TextStyle(fontSize: 30, color: Colors.black),
                        ),
                      ),
                      const Text(
                        'Digite seu e-mail abaixo para redefini-la',
                        style: TextStyle(color: Colors.black, fontSize: 17),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          enabledBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black)),
                          labelStyle: const TextStyle(color: Colors.black),
                          labelText: 'Email',
                          prefixIcon: const Icon(
                            Icons.mail,
                            color: Colors.black,
                          ),
                        ),
                        style: const TextStyle(color: Colors.black),
                        keyboardType: TextInputType.emailAddress,
                        controller: controller.emailController,
                        onSaved:
                            ((value) {} ),
                        validator: ((value) {
                          return controller.validateEmail(value!);
                        }),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      SizedBox(
                        width: 300,
                        child: ElevatedButton(
                          onPressed: () => controller.sendResetEMail(),
                          style: ButtonStyle(
                            shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                            backgroundColor:
                                MaterialStateProperty.all(Colors.amber),
                            padding:
                                MaterialStateProperty.all(const EdgeInsets.all(20)),
                          ),
                          child: const Text(
                            "Redefinir minha Senha",
                            style: TextStyle(fontSize: 16, color: Colors.black),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}


/*  child: ListView(
              controller: scrollController,
              shrinkWrap: true,
              children: [],
            ), */