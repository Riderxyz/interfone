import 'package:evox/app/modules/historico/views/historico_info_view.dart';
import 'package:get/get.dart';

import '../modules/chamada/bindings/chamada_binding.dart';
import '../modules/chamada/views/chamada_view.dart';
import '../modules/config/bindings/config_binding.dart';
import '../modules/config/views/config_view.dart';
import '../modules/historico/bindings/historico_binding.dart';
import '../modules/historico/views/historico_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.LOGIN;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => const LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.HISTORICO,
      page: () => const HistoricoView(),
      binding: HistoricoBinding(),
    ),
     GetPage(
      name: _Paths.HISTORICO_INFO,
      page: () => const HistoricoInfoView(),
      binding: HistoricoBinding(),
    ),
    GetPage(
      name: _Paths.CHAMADA,
      page: () => const ChamadaView(),
      binding: ChamadaBinding(),
    ),
    GetPage(
      name: _Paths.CONFIG,
      page: () => const ConfigView(),
      binding: ConfigBinding(),
    ),
  ];
}
