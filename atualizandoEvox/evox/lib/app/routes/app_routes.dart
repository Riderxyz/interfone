part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const LOGIN = _Paths.LOGIN;
  static const HISTORICO = _Paths.HISTORICO;
  static const QR_CODE = _Paths.QR_CODE;
  static const HISTORICO_INFO = _Paths.HISTORICO_INFO;
  static const CHAMADA = _Paths.CHAMADA;
  static const CONFIG = _Paths.CONFIG;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const HISTORICO = '/historico';
  static const QR_CODE = '/qr-code';
  static const HISTORICO_INFO = '/historico-info';
  static const CHAMADA = '/chamada';
  static const CONFIG = '/config';
}
