class PushNotificationData {
  String? visitorsPhoto;
  //String? action;
  /// Nome do visitante
  String? nome;
  /// ID de usuario do proprietario da casa
  String? proprietarioId;
  /// id aleatorio para notificação ser ativa
  String? id;
  /// texto para possivel mensagem
  String? body;
  /// titulo para possivel mensagem
  String? title;
  /// Id do canal em que a notificção irá ser mostrada
  String? channelId;
  /// ação de click da notificação
  String? clickAction;

  PushNotificationData(
      {this.visitorsPhoto,
      // this.action,
      this.nome,
      this.proprietarioId,
      this.id,
      this.body,
      this.title,
      this.channelId,
      this.clickAction});

  PushNotificationData.fromJson(Map<String, dynamic> json) {
    visitorsPhoto = json['visitorsPhoto'];
    nome = json['nome'];
    proprietarioId = json['proprietarioId'];
    id = json['id'];
    body = json['body'];
    title = json['title'];
    channelId = json['channelId'];
    clickAction = json['clickAction'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['visitorsPhoto'] = visitorsPhoto;
    data['nome'] = nome;
    data['proprietarioId'] = proprietarioId;
    data['id'] = id;
    data['body'] = body;
    data['title'] = title;
    data['channelId'] = channelId;
    data['clickAction'] = clickAction;
    return data;
  }
}