import 'endereco.model.dart';

class Usuario {
  String? nome;
  String? celular;
  String? id;
  String? photo;
  String? tokenChamada;
  String? email;
  int? dataCadastro;
  int? dataAtualizacao;
  Endereco? endereco;

  Usuario(
      {this.nome,
      this.celular,
      this.id,
      this.photo,
      this.tokenChamada,
      this.email,
      this.dataCadastro,
      this.dataAtualizacao,
      this.endereco});

  Usuario.fromJson(Map<String, dynamic> json) {
    nome = json['nome'];
    celular = json['celular'];
    id = json['id'];
    photo = json['photo'];
    tokenChamada = json['tokenChamada'];
    email = json['email'];
    dataCadastro = json['dataCadastro'];
    dataAtualizacao = json['dataAtualizacao'];
    endereco = json['endereco'] != null
        ? new Endereco.fromJson(json['endereco'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nome'] = this.nome;
    data['celular'] = this.celular;
    data['id'] = this.id;
    data['photo'] = this.photo;
    data['tokenChamada'] = this.tokenChamada;
    data['email'] = this.email;
    data['dataCadastro'] = this.dataCadastro;
    data['dataAtualizacao'] = this.dataAtualizacao;
    if (this.endereco != null) {
      data['endereco'] = this.endereco!.toJson();
    }
    return data;
  }
}
