class Chamada {
  /**
   * Data em que a chamada foi efetuada */  
  late int data;
    /**
   * Data do Fim da chamada */  
  late int? endTime;
    /**
   * Data do Inicio da chamada */  
  late int? startTime;
    /**
   * Foto do visitante */  
  late String img;
    /**
   * A chamada foi finalizada? */  
  late bool isChamadaFinalizada;
  /**
   * Nome do Visitante  */
  late String nome;
    /**
   * Tempo da chamada */  
  late int tempoChamada;
  /**
   * Latitude do visitante */
  late int? localizacaoLat;
    /**
   * Longitude do visitante */
  late int? localizacaoLong;

  Chamada(
      {required this.data,
      required this.endTime,
      required this.startTime,
      required this.img,
      required this.isChamadaFinalizada,
      required this.nome,
      required this.tempoChamada,
      required this.localizacaoLat,
      required this.localizacaoLong});

  Chamada.fromJson(Map<String, dynamic> json) {
    data = json['data'];
    endTime = json['endTime'] == null ? null : json['endTime'];
    startTime = json['startTime'] == null ? null : json['startTime'];
    img = json['img'];
    isChamadaFinalizada = json['isChamadaFinalizada'];
    nome = json['nome'];
    tempoChamada = json['tempoChamada'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.data;
    data['endTime'] = this.endTime;
    data['startTime'] = this.startTime;
    data['img'] = this.img;
    data['isChamadaFinalizada'] = this.isChamadaFinalizada;
    data['nome'] = this.nome;
    data['tempoChamada'] = this.tempoChamada;
    return data;
  }
}
