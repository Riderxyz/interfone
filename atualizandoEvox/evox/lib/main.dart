import 'dart:developer';

import 'package:evox/app/services/auth.service.dart';
import 'package:evox/app/services/data.service.dart';
import 'package:evox/app/services/notification.service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';

import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:permission_handler/permission_handler.dart';
import 'firebase_options.dart';
import 'app/routes/app_pages.dart';

Future<void> initForAgora() async {
  print('ESTOU AQUI! INICIANDO PERMISSÕES');
  await [
    Permission.microphone,
    Permission.phone,
    Permission.storage,
    Permission.camera
  ].request();
}

initServices() async {
  await initializeDateFormatting('pt_BR', null);
  Intl.defaultLocale = 'pt_BR';
  print('starting services ...');
  await Get.putAsync(() => DataService().init());
  await Get.putAsync(() => NotificationService().init());
  await Get.putAsync(() => AuthService().init());
  print('All services started...');
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
          [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown])
      .then((value) => null);
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
/* 
  Future.delayed(const Duration(seconds: 10)).then((value) {
    AwesomeNotifications().createNotification(
      content: NotificationContent(
        id: 1,
        channelKey: 'ligacao',
        title: 'Background action',
        wakeUpScreen: true,
        category: NotificationCategory.Call,
        fullScreenIntent: true,
        autoDismissible: false,
        actionType: ActionType.Default,
        body: 'This notification was created by a background action',
      ),
      actionButtons: [
        NotificationActionButton(
            key: 'accept', label: 'Atender', autoDismissible: false),
        NotificationActionButton(
            key: 'decline', label: 'Rejeitar', autoDismissible: true),
      ],
    );
  });
*/
 await initServices();
  runApp(
    GetMaterialApp(
      title: "Evox",
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
      theme: ThemeData.dark(),
    ),
  );
}
