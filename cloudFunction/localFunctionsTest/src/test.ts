import * as https from "https";
import * as db from "firebase-admin";
import * as serviceAccount from "./serviceAccountKey.json";
export const deleteCollection = async () => {
  let agoraSecret = {
    key: "90820edb8b364766ae34babb0e4182b5",
    secret: "28f09a1659384b51877137759abefbd9",
  };
  db.initializeApp({
    credential: db.credential.cert((serviceAccount as any).default),
    databaseURL: "https://interfone-a2139-default-rtdb.firebaseio.com",
  });
  // Concatenate customer key and customer secret and use base64 to encode the concatenated string
  const plainCredential = agoraSecret.key + ":" + agoraSecret.secret;
  // Encode with base64
  const encodedCredential = Buffer.from(plainCredential).toString("base64");
  const authorizationField = "Basic " + encodedCredential;
console.log(authorizationField);

  const options = {
    hostname: "api.agora.io",
    port: 443,
    path: "/dev/v1/projects",
    method: "GET",
    headers: {
      Authorization: authorizationField,
      "Content-Type": "application/json",
    },
  };

  const req = https.request(
    "api.agora.io/dev/v1/projects:443",
    {
      headers: {
        authorizationField,
        "Content-Type": "application/json",
      },
      method: "GET",
    },
    (res) => {
      console.log(`Status code: ${res.statusCode}`);

      res.on("data", (d: any) => {
        process.stdout.write(d);
      });
    }
  );
  req.end();
};
