/* eslint-disable linebreak-style */
/* eslint-disable camelcase */

export interface NotificationInterface {
    to?: string;
    mutable_content?: boolean;
    content_available?: boolean;
    priority?: string;
    data?: Data;
}


export interface Content {
    id: number;
    channelKey: string;
    title: string;
    body: string;
    notificationLayout?: 'Default' | 'BigPicture'|'BigText' | 'Inbox' | 'ProgressBar' | 'Messaging'| 'MediaPlayer'
    largeIcon?: string;
    bigPicture?: string;
    showWhen?: boolean;
    autoCancel?: boolean;
    privacy?: 'Secret' |'Private' | 'Public';
}

export interface ActionButton {
    key: string;
    label: string;
    autoCancel: boolean;
    buttonType: 'Default' | 'InputField' | 'KeepOnTop' | 'DisabledAction';
}

export interface Schedule {
    timeZone: string;
    hour: string;
    minute: string;
    second: string;
    millisecond: string;
    allowWhileIdle: boolean;
    repeat: boolean;
}

export interface Data {
    content: Content;
    actionButtons?: ActionButton[];
    schedule?: Schedule;
}