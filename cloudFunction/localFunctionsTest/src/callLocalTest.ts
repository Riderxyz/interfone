import * as admin from "firebase-admin";
import * as serviceAccount from "./serviceAccountKey.json";

export const callLocalTest = async () => {
  admin.initializeApp({
    credential: admin.credential.cert((serviceAccount as any).default),
    databaseURL: "https://interfone-a2139-default-rtdb.firebaseio.com",
  });

  const pushTokenArr =
    "eT6HKBQERay-nZMOCbvruG:APA91bFWF52IOgtIBwbLoAH7xhnIyaY96AKT9F2nhdDoRjRvCNHLvGAxsm5qkkM_IpkNJb25-GE1lHwJwXgZon1AtDAuWZJ4Z6FJhC3d9zqGkLZFVVe2Ki6_U37ea-yqQkmD7Ur0TfMS";
  admin
    .messaging()
    .sendToDevice(pushTokenArr, {
      data: {
      action: "Incoming call",
      nome:"john",
      id:'qsçkdsjdjasloiaolfhajlosjhda',
      body:'loias',
      title:"chamadaX",
      proprietarioId:"qwers",
      channelId:"ligacao"
      },
      
      
/*       notification: {
        title: "X-X",
        body: "teste de ligação",
      }, */
    })
    .finally(() => {
      console.log("FinalizouXAS");
    });
  console.clear();
};

/*


0: "ebtF-fXESYKtndclfQrtY6:APA91bGgQDa89O-uUE1KpODTE-1AwJlVLuYkWV2VBqKg-sUM6htjMCWkfDatMrk7KS8unqN-S8HHVUQ1Gw0hm6_Ms6WDM2-6af4gNO2hDke34suazq44C2hOjvpcqK1pvxj7_1CSnVQ1"1: "cQNS6lQQRsybyFZlIJv1Ny:APA91bHzn_XgcOgcaW98YriSorxlfgeYlZdp8XhtJjGElSZzQHAQI461h0zEYB3dyD5fUt47kjgpzSxaW1Bwv75i6fyp1xxyJ8mfY37-EUNxoemfPA8iIplNWDnyfJ-tVfmaOT1uB7Fn"2: "fPxhOqPrRdeys_JCZxYGVH:APA91bGyjlPkadc7vMi0P39LdPi0lxOrkQQfS83M2rvj9yigkH6fU1jo2mMVZjU33MPbObLTkE--6m8l00kdsJX3PVrfZnxKHi3Qd2xAUwHWiHwMv6OyV1It3gT__xPOmtN4JEBK_EFb"length: 3[[Prototype]]: Array(0)
*/
