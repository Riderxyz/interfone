import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import { BufferOptions, TDocumentDefinitions } from "pdfmake/interfaces";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
export const testgenerateQrCodePDF = async () => {
  const pdfScheme: TDocumentDefinitions = {
    pageSize: "A4",
    background: function () {
      return {
        canvas: [
          {
            type: "rect",
            x: 0,
            y: 0,
            w: 595.28,
            h: 841.89,
            color: "#000000",
          },
        ],
      };
    },
    content: [
      {
        text: "I N T E R F O N E",
        fontSize: 50,
        bold: true,
        alignment: "center",
        color: "#FFFFFF",
        margin: [0, 0, 0, 40],
      },
      {
        text: "Bem Vindo(a)",
        fontSize: 30,
        alignment: "center",
        color: "#FFFFFF",
        margin: [0, 0, 0, 20],
      },
      {
        text: "Para ligar para este usuario, aponte a camera para o codigo abaixo",
        fontSize: 30,
        alignment: "center",
        color: "#FFFFFF",
        margin: [0, 0, 0, 90],
      },
      {
        alignment: "center",
        columns: [
          {
            qr:
              "https://visitanteprototipo.netlify.app/#/home/" +
              "ajksjkajsjaksjksjkasjaksjakjaksjaksajqoyiqwtuieyw",
            fit: 350,
            foreground: "white",
            background: "black",
            margin: [0, 0, 0, 20],
          },
          // generateQRCode:
        ],
      },
    ],
  };
  let buff: BufferOptions;
 //let tes = pdfMake.createPdf(pdfScheme).getStream().on()
 
 /* .on('finish', ((res) => {
    console.log('terminei ?');
    
 })) */
};
