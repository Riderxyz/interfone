import * as Agora from "agora-access-token";
export const rtmToken = (() => {
    const appID = "c292ed23b06c45688833426a3703a278";
    const appCertificate = "d65e6ab85fb648858844510a17ddec14";
    const userAccount = 'onegai';
    const role = Agora.RtmRole.Rtm_User;
    const expirationTimeInSeconds = 3600;
    const currentTimestamp = Math.floor(Date.now() / 1000);
    const expirationTimestamp = currentTimestamp + expirationTimeInSeconds;
  
    const token = Agora.RtmTokenBuilder.buildToken(appID, appCertificate, userAccount, role, expirationTimestamp);
   console.log(token);
   
}) 