import * as functions from "firebase-functions";
import * as Agora from "agora-access-token";
const cors = require("cors")({ origin: true });
export const generateRTMToken = (
  request: functions.https.Request,
  response: functions.Response
) => {
  cors(request, response, () => {
    const appID = "c292ed23b06c45688833426a3703a278";
    const appCertificate = "d65e6ab85fb648858844510a17ddec14";
    const userAccount = request.body.user;
    const role = Agora.RtmRole.Rtm_User;
    const expirationTimeInSeconds = 3600;
    const currentTimestamp = Math.floor(Date.now() / 1000);
    const expirationTimestamp = currentTimestamp + expirationTimeInSeconds;
  
    const token = Agora.RtmTokenBuilder.buildToken(appID, appCertificate, userAccount, role, expirationTimestamp);
   
    response.status(200).send({ token });
});
};
