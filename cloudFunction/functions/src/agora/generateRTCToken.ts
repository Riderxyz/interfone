import * as functions from "firebase-functions";
import * as Agora from "agora-access-token";
export const generateRTCToken = (
  data: any,
  context: functions.https.CallableContext
) => {
  const appID = "c292ed23b06c45688833426a3703a278";
  const appCertificate = "d65e6ab85fb648858844510a17ddec14";
  const uid = Math.floor(Math.random() * 100000);
  const expirationTimeInSeconds = 3600;
  const currentTimestamp = Math.floor(Date.now() / 1000);
  const privilegeExpiredTs = currentTimestamp + expirationTimeInSeconds;
  /**
   *
   * Agora.RtcRole.PUBLISHER=> O visitante sempre será o publicador, aquele que cria a chamada
   *  Agora.RtcRole.SUBSCRIBER => O proprietario sempre será o inscrito, aquele que só pode entrar nas chamadas
   */
  const role = data.isPublisher
    ? Agora.RtcRole.PUBLISHER
    : Agora.RtcRole.SUBSCRIBER;
  const channel = data.channel;
functions.logger.info(data)
  const token = Agora.RtcTokenBuilder.buildTokenWithUid(
    appID,
    appCertificate,
    channel,
    uid,
    role,
    privilegeExpiredTs
  );
  const retorno = { uid, token };
  return retorno;
};
