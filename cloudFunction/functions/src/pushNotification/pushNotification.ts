
import * as admin from "firebase-admin";
import * as functions from "firebase-functions";

const cors = require("cors")({ origin: true });
export const pushNotification = (
  request: functions.https.Request,
  response: functions.Response
) => {
  cors(request, response, () => {
    console.log("ativando cors");
    interface PayLoadInterface {
      notification: {
        title: string;
        body: string;
        click_action?: string;
        icon: string;
        color: string;
        badge?: string;
        tap: "false" | "true";
        tag?: string;
      };
      data?: any;
    }

    interface messageObj {
      pushMessage: {
        title: string;
        body: string;
        click_action?: string;
        icon: string;
        color: string;
        badge?: string;
        tap: "false" | "true";
        tag?: string;
      };
      token: string[];
    }
    const messageArr: messageObj = JSON.parse(request.body);
    const SendPushNotification = (token: string[]) => {
      const payload: PayLoadInterface = {
        notification: messageArr.pushMessage,
      };
      console.log("O QUE EXISTE AQUI DENTRO DA FUNÇÃO", token);
      admin
        .messaging()
        .sendToDevice(token, payload)
        .then((res) => {
          response.status(200).send('Envio concluido');
        })
        .catch((err) => {
          response.status(400).send('Erro ao enviar função');
        });
    };
    console.log("Okay, como devo tratar isso então?", messageArr);
    console.log("o que existe detro do payload", messageArr.pushMessage);
    console.log("o que existe dento do array de token?", messageArr.token);
    SendPushNotification(messageArr.token);
  });
};
