import * as admin from "firebase-admin";
//import * as pushNotification from "firebase-admin/messaging";
import * as functions from "firebase-functions";
const cors = require("cors")({ origin: true });
export const ligacaoNotificationHttps = (
  request: functions.https.Request,
  response: functions.Response
) => {
  cors(request, response, async () => {
    functions.logger.info("Iniciando função de ligação.", {
      structuredData: true,
    });

    let body: {
      userId: string;
      pushTokenArr: string[];
      visitante: string;
      data: {
        title: string;
        body: string;
        id: string;
        proprietarioId: string;
        visitorsPhoto: string;
        nome: string;
        clickAction: string;
        channelId: string;
      };
    } = request.body;

    const data: admin.messaging.DataMessagePayload = request.body.data;
    const callNotification: admin.messaging.MulticastMessage = {
      tokens: body.pushTokenArr,
      data,
/*       notification:{
        title: 'Você esta recebendo uma ligação de ' + body.data.nome,
        body: 'Atenda a ligação'
      } */
    };
    try {
      functions.logger.info(
        "Trying to send PushNotifications. Starting now the *sendMulticast* function ↓",
        {
          structuredData: true,
        }
      );
      const result = await admin.messaging().sendMulticast(callNotification);
      functions.logger.info(
        `${result.successCount} messages were sent successfully`,
        {
          structuredData: true,
        }
      );
      response
        .status(200)
        .send({
          status:`${result.successCount} messages were sent successfully`,
          cloudFunctionversion: '0.0.9'
        });
    } catch (error) {
      console.error("Error sending messages:", error);
      functions.logger.error("Error de envio de Notificação.", {
        structuredData: true,
      });
      functions.logger.error(error, {
        structuredData: true,
      });
      response.status(500).send("Error sending messages");
    }

    response.status(200).send({
      status: "Finalizado",
    });
  });
};
