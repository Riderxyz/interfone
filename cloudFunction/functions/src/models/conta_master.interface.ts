import { UsuarioInteface } from "./usuario.interface";
import { EnderecoInterface } from "./endereco.interface";
import * as uuid from "uuid-random";
export interface masterConta {
  endereco?: EnderecoInterface;
  tokenChamada: string;
  email: string;
  id: string;
  data_atualizacao: number;
  data_cadastro: number;
  contasRelacionadas: UsuarioInteface[];
}

export const newMasterConta = () => {
  let retorno: masterConta = {
    tokenChamada: uuid(),
    email: "",
    data_atualizacao: 0,
    data_cadastro: 0,
    id: "",
    contasRelacionadas: [],
  };
  return retorno;
};
