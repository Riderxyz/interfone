export interface historicoChamada {
  img?: string;
  tempoChamada?: any;
  nome: string;
  endTime: any;
  data: number;
  isChamadaFinalizada: boolean;
  startTime: any;
}
