import { EnderecoInterface } from './endereco.interface';
export interface LegacyUsuarioInteface {
  nome: string;
  email: string;
  celular?: string;
  endereco?:EnderecoInterface;
  id: string;
  tokenChamada: string;
}


