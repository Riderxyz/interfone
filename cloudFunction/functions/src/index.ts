import { generateRTMToken } from "./agora/generateRTMToken";
import { generateRTCToken } from "./agora/generateRTCToken";
import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import { pushNotification } from "./pushNotification/pushNotification";
import { ligacaoNotificationHttps } from "./pushNotification/ligacao";
import { addUser } from "./userAdmin/legacy_addUser";
import { deleteUser } from "./userAdmin/deleteUser";
admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: "https://interfone-a2139-default-rtdb.firebaseio.com",
});

export const SendNotification = functions.https.onRequest(
  (request, response) => {
    pushNotification(request, response);
  }
);
export const agoraGenerateRTCToken = functions.https.onCall(
  (data, context) => {
    return generateRTCToken(data, context);
  }
);
export const agoraGenerateRTMToken = functions.https.onRequest(
  (request, response) => {
    generateRTMToken(request, response);
  }
);
export const LigacaoNotification = functions.https.onRequest(
  (request, response) => {
    functions.logger.info("Chamando função de ligação", {
      structuredData: true,
    });
    return ligacaoNotificationHttps(request, response);
  }
);
export const addUserToFirebase = functions.https.onRequest(
  (request, response) => {
    addUser(request, response);
  }
);
export const deleteUserFromFirebase = functions.https.onRequest(
  (request, response) => {
    deleteUser(request, response);
  }
);

export const generateQRCodeForDownload = functions.https.onRequest(
  (request, response) => {
    response.status(200).send('Desabilitada por agr')
    //generateQRCode(request, response);
  }
);

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
