import * as admin from "firebase-admin";
import * as functions from "firebase-functions";
import * as uuid from "uuid-random";
const cors = require("cors")({ origin: true });
export const addUser = (
  request: functions.https.Request,
  response: functions.Response
) => {
  cors(request, response, () => {
    console.log("ativando cors");
    admin
      .auth()
      .createUser({
        email: request.body.email,
        emailVerified: true,
        password: request.body.password,
        displayName: request.body.name,
      })
      .then((firebaseUser) => {
        admin
          .firestore()
          .collection("usuarios")
          .doc(firebaseUser.uid)
          .set({
            nome: request.body.name,
            email: request.body.email,
            id: firebaseUser.uid,
            data_atualizacao: admin.firestore.Timestamp.fromDate(
              new Date()
            ).toMillis(),
            data_cadastro: admin.firestore.Timestamp.fromDate(
              new Date()
            ).toMillis(),
            tokenChamada: uuid(),
          })
          .then(() => {
            response.status(200).send({
              response: 200,
              msg: "Usuario Adicionado com sucesso",
            });
          })
          .catch((err) => {
            response.status(500).send({
              response: 500,
              localizacao:'criação no banco de dados',
              errorDesc: err,
              msg: "Usuario não pode ser adicionado. tente novamente",
            });
          });
      })
      .catch((err) => {
        response.status(500).send({
          response: 500,
          localizacao:'criação auth System do Firebase',
          errorDesc: err,
          msg: "Usuario não pode ser adicionado. tente novamente",
        });
      });
  });
};
