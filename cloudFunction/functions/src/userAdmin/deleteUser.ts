import * as admin from "firebase-admin";
import * as functions from "firebase-functions";

const cors = require("cors")({ origin: true });
export const deleteUser = (
  request: functions.https.Request,
  response: functions.Response
) => {
  cors(request, response, () => {
    admin
      .firestore()
      .collection("usuarios")
      .doc(request.body.id)
      .delete()
      .then(async () => {
        await deleteSubcolections(request.body.id);
        admin
          .auth()
          .deleteUser(request.body.id)
          .then(async () => {
            functions.logger.log(
              "Sucesso em deletar usuario de ID: " + request.body.id
            );

            response.status(200).send({
              response: 200,
              msg: "Usuario deletado com sucesso",
            });
          })
          .catch((err) => {
            functions.logger.log(
              "Erro em deletar usuario de ID: " + request.body.id
            );
            functions.logger.error(err);
            response.status(500).send({
              response: 500,
              errorDesc: err,
              msg: "Usuario não pode ser deletado",
            });
          });
      })
      .catch((err) => {
        functions.logger.error(err);
        response.status(500).send({
          response: 500,
          errorDesc: err,
          msg: "Usuario não pode ser deletado",
        });
      });
  });
};

const deleteSubcolections = async (userId: string) => {
  const isChamadaAtual = await admin
    .firestore()
    .collection("usuarios")
    .doc(userId)
    .collection("chamadaAtual")
    .limit(1)
    .get();
  const isHistoricoChamadas = await admin
    .firestore()
    .collection("usuarios")
    .doc(userId)
    .collection("historicoChamadas")
    .limit(1)
    .get();
  const isNotificationToken = await admin
    .firestore()
    .collection("usuarios")
    .doc(userId)
    .collection("notificationToken")
    .limit(1)
    .get();

  if (isChamadaAtual.size > 0) {
    const snapshot = await admin
      .firestore()
      .collection("usuarios")
      .doc(userId)
      .collection("chamadaAtual")
      .get();
    const batch = admin.firestore().batch();
    snapshot.docs.forEach((doc) => {
      batch.delete(doc.ref);
    });
    await batch.commit();
  }
  if (isHistoricoChamadas.size > 0) {
    const snapshot = await admin
      .firestore()
      .collection("usuarios")
      .doc(userId)
      .collection("historicoChamadas")
      .get();
    const batch = admin.firestore().batch();
    snapshot.docs.forEach((doc) => {
      batch.delete(doc.ref);
    });
    await batch.commit();
  }
  if (isNotificationToken.size > 0) {
    const snapshot = await admin
      .firestore()
      .collection("usuarios")
      .doc(userId)
      .collection("notificationToken")
      .get();
    const batch = admin.firestore().batch();
    snapshot.docs.forEach((doc) => {
      batch.delete(doc.ref);
    });
    await batch.commit();
  }
};
