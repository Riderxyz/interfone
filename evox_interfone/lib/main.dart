import 'dart:isolate';
import 'dart:ui';

import 'package:evox_interfone/app/pages/chamada/backgroundCall/callKeepConfig.dart';
import 'package:evox_interfone/app/pages/chamada/backgroundCall/callKeepEventSwitcher.dart';
import 'package:evox_interfone/app/pages/chamada/chamada_binding.dart';
import 'package:evox_interfone/app/pages/chamada/chamada_view.dart';
import 'package:evox_interfone/app/services/analytics.service.dart';
import 'package:evox_interfone/app/services/auth.service.dart';
import 'package:evox_interfone/app/services/data.service.dart';
import 'package:evox_interfone/app/services/notification.service.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_callkit_incoming/entities/call_event.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';

import 'package:get/get.dart';
import 'package:evox_interfone/app/models/pushNotificationData.model.dart';
import 'package:evox_interfone/awesomeNotificationConfig.dart';
import 'package:evox_interfone/firebase_options.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'app/routes/app_pages.dart';

Future<void> initForAgora() async {
  print('ESTOU AQUI! INICIANDO PERMISSÕES');
  await [
    Permission.microphone,
    Permission.phone,
    Permission.storage,
    Permission.camera
  ].request();
}

@pragma('vm:entry-point')
Future<void> _fcmBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  PushNotificationData pushData = PushNotificationData.fromJson(message.data);
  print('CHEGUEI NO BACKGROUND ! ! !');
  print(message.data);
  print(pushData.toJson());
  String currentAction = message.data['action'];
  if (currentAction.toLowerCase() == "incoming call") {
    final params = callKeepParams(pushData);
    await FlutterCallkitIncoming.showCallkitIncoming(params);
    FlutterCallkitIncoming.onEvent.listen((event) {
      print(event);
      onCallKeepEventReceived(event, pushData);
    });
  }
}

Future<void> initService() async {
  initializeDateFormatting();
  Intl.defaultLocale = 'pt_BR';
  print('starting services ...');
  await Get.putAsync(() => AnalyticsService().init());
  await Get.putAsync(() => DataService().init());
  await Get.putAsync(() => NotificationService().init());
  await Get.putAsync(() => AuthService().init());
  print('All services started...');
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations(
          [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown])
      .then((value) => null);
  await AwesomeNotificationConfigController.initializeLocalNotifications();
  await AwesomeNotificationConfigController.initializeIsolateReceivePort();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  FirebaseMessaging.onBackgroundMessage(_fcmBackgroundHandler);
  await FlutterCallkitIncoming.endAllCalls();
  await initService();
  runApp(
    GetMaterialApp(
      title: "Evox Interfone",
      initialRoute: AppPages.INITIAL,
      navigatorKey: DefaultFirebaseOptions.navigatorKey,
      getPages: AppPages.routes,
      theme: ThemeData.dark(),
    ),
  );
  initForAgora();
}
