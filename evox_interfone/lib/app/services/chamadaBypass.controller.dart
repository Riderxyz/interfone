import 'dart:async';

import 'package:evox_interfone/app/enviroment.dart';
import 'package:evox_interfone/app/pages/chamada/chamada_view.dart';
import 'package:flutter/animation.dart';
import 'package:evox_interfone/app/services/data.service.dart';
import 'package:get/get.dart';

class ChamadaBypassCtrl extends SuperController {
  late Timer onDetachedTimer;
  late Timer onInactiveTimer;
  late Timer onPausedTimer;
  late Timer onResumedTimer;
  final DataService dataSrv = Get.find<DataService>();
  Future<ChamadaBypassCtrl> init() async {
    print('ChamadaBypassCtrl INICIOU!');
    return this;
  }

/* 

Timer.periodic(Duration(seconds: 1), (timer) {} );
Timer.periodic(Duration(seconds: 1), (timer) {} );
Timer.periodic(Duration(seconds: 1), (timer) {} );
Timer.periodic(Duration(seconds: 1), (timer) {} );
 */
  @override
  void onDetached() {
    print('object');
    int i = 0;
    print('App em background');
    onDetachedTimer = Timer.periodic(Duration(seconds: 1), (timer) {
      i++;
      print('Isso aqui ja rodou ' + i.toString() + ' vezes em onDetached');
    });
  }

  @override
  void onInactive() {
    this.dataSrv.appCurrentStatus = AppCurrentStatus.inactive;
    int i = 0;
    print('App terminado');
    onInactiveTimer = Timer.periodic(Duration(seconds: 1), (timer) {
      i++;
      print('Isso aqui ja rodou ' + i.toString() + ' vezes em onInactive');
      print(dataSrv.appCurrentStatus);
    });
  }

  @override
  void onPaused() {
    this.dataSrv.appCurrentStatus = AppCurrentStatus.inactive;
    int i = 0;
    print('App em background');
    onPausedTimer = Timer.periodic(Duration(seconds: 1), (timer) {
      i++;
      print('Isso aqui ja rodou ' + i.toString() + ' vezes em onPaused');
      print(dataSrv.appCurrentStatus);
    });
  }

  @override
  void onResumed() {
    //int i = 0;
    print('App em F O R E G R O U N D ! ! !');
    this.dataSrv.appCurrentStatus = AppCurrentStatus.active;
    //this.dataSrv.currentCallStatus.value = CallStatus.accepted;
    print(dataSrv.appCurrentStatus);
    try {
      onDetachedTimer.cancel();
    } catch (e) {
      print(e);
    }
    try {
      onInactiveTimer.cancel();
    } catch (e) {
      print(e);
    }
    try {
      onPausedTimer.cancel();
    } catch (e) {
      print(e);
    }
  }

@override
  void onHidden() {
    // TODO: implement onHidden
  }
}

