import 'dart:async';
import 'dart:developer';
//import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:evox_interfone/app/enviroment.dart';
import 'package:evox_interfone/app/models/chamada.model.dart';
import 'package:evox_interfone/app/models/pushNotificationData.model.dart';
import 'package:evox_interfone/app/models/usuario.model.dart';

import 'package:get/get.dart';
import 'package:uuid/uuid.dart';

class DataService extends GetxService {
  final token = ''.obs;
  var count = 0.obs;
  Usuario currentUsuario = Usuario(
    nome: '',
    celular: '',
    id: '',
    photo: '',
    tokenChamada: '',
    email: '',
    dataCadastro: 0,
    dataAtualizacao: 0,
    endereco: null,
  );
  Chamada selectedChamadaForDetail = Chamada(
      data: 0,
      endTime: 0,
      startTime: 0,
      img: '',
      isChamadaFinalizada: false,
      nome: '',
      tempoChamada: 0,
      localizacaoLat: 0,
      localizacaoLong: 0);
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  AppCurrentStatus appCurrentStatus = AppCurrentStatus.active;
  Rx<CallStatus> currentCallStatus = CallStatus.nullStatus.obs;
  Future<DataService> init() async {
    print('Só pra clamar o debug');
    return this;
  }

  saveUserData() {
    _firestore
        .collection('usuarios')
        .doc(currentUsuario.id)
        .set(currentUsuario.toJson())
        .then((value) {
      debugger();
    });
  }

  updateUserQRCodeToken(String qrCodeToken) {
    _firestore.collection('usuarios').doc(currentUsuario.id).update({
      'tokenChamada': qrCodeToken,
    }).then((value) {
      print('Funcionou?');
    });
  }

  DocumentReference<Usuario> get userData {
    return _firestore
        .collection('usuarios')
        .doc(currentUsuario.id)
        .withConverter<Usuario>(
            fromFirestore: (snapshot, opt) =>
                Usuario.fromJson(snapshot.data()!),
            toFirestore: (masterConta, opt) => masterConta.toJson());
  }

  CollectionReference<Chamada> get getUltimasChamada {
    return _firestore
        .collection('usuarios')
        .doc(currentUsuario.id)
        .collection('historicoChamadas')
        .withConverter<Chamada>(
            fromFirestore: (snapshot, opt) =>
                Chamada.fromJson(snapshot.data()!),
            toFirestore: (profile, opt) => profile.toJson());
  }

resetPort() {
     /* IsolateNameServer.removePortNameMapping('portForAgoraIoCall');
    IsolateNameServer.registerPortWithName(
        _portForCall.sendPort, 'portForAgoraIoCall'); */
  }
}
