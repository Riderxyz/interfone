import 'dart:async';
import 'dart:io';

import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:evox_interfone/app/models/pushNotificationData.model.dart';
import 'package:evox_interfone/app/pages/chamada/backgroundCall/callKeepConfig.dart';
import 'package:evox_interfone/app/pages/chamada/backgroundCall/callKeepEventSwitcher.dart';
import 'package:flutter/material.dart';
import 'package:android_id/android_id.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:get/get.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class NotificationService extends GetxService {
  final FirebaseMessaging messaging = FirebaseMessaging.instance;
  final FirebaseFirestore firestore = FirebaseFirestore.instance;
  //final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
  final _androidIdPlugin = const AndroidId();

  var identifier = '';
  late Map<String, dynamic> pushRetrivedData;
  Future<NotificationService> init() async {
    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: true,
      badge: true,
      carPlay: false,
      criticalAlert: true,
      provisional: true,
      sound: true,
    );
    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('iniciei o serviço de Push');
    }
    FirebaseMessaging.onMessageOpenedApp.listen((event) {
      print('Ao clicar na linha 31');
      print(event);
    });


    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print('Recebi uma mensagem aqui em foregrSound:}');
      print('Data: ${message.data['channelId']}');
      var test = message.data;
      PushNotificationData pushRetrivedData =
          PushNotificationData.fromJson(message.data);
      if (pushRetrivedData.channelId == 'ligacao') {
        final params = callKeepParams(pushRetrivedData);
       FlutterCallkitIncoming.showCallkitIncoming(params)
            .then((value) => FlutterCallkitIncoming.onEvent.listen((event) {
                  onCallKeepEventReceived(event, pushRetrivedData);
                }));
      }
    });
    try {
      if (Platform.isAndroid) {
        _androidIdPlugin.getId().then((androidData) {
          if (androidData != null) {
            identifier = androidData; //UUID for Android
          }
        }).catchError((onError) {
          print(onError);
        });
      }
    } catch (e) {
      print('Deu tudo errado ao pegar o UUID do celular');
      print(e);
    }
    return this;
  }

  fcmToken(String uid) {
    FirebaseMessaging.instance.getToken().then((value) {
      print(identifier);
      print('token de mensagem =  ' + value.toString());
      firestore
          .collection('usuarios')
          .doc(uid)
          .collection('notificationToken')
          .doc(identifier)
          .set({
        'platorm': Platform.isAndroid ? 'Android' : 'IOS',
        'criadoEm': DateTime.now().millisecondsSinceEpoch,
        'token': value
      });
      print(value);
    }).catchError((onError) {
      print('object');
      print(onError);
    });
  }

  testeSonoro() {
    print('asasssa');
/*     AwesomeNotifications()
        .showNotificationConfigPage(channelKey: 'basic_channel');
    AwesomeNotifications().requestPermissionToSendNotifications(); */
    Timer(
        const Duration(seconds: 1),
        () => AwesomeNotifications().createNotification(
              content: NotificationContent(
                  title: 'teste',
                  body: 'tem pobre ligando pra mim',
                  notificationLayout: NotificationLayout.Default,
                  displayOnBackground: true,
                  //criticalAlert: true,
                  backgroundColor: Colors.amber,
                  color: Colors.black,
                  id: 554576,

                  //wakeUpScreen: true,
                  channelKey: 'basic_channel'),
            ));
  }
}
