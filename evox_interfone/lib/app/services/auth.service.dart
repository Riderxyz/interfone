// ignore_for_file: import_of_legacy_library_into_null_safe, unused_import, argument_type_not_assignable_to_error_handler, unused_local_variable

import 'dart:async';
import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:evox_interfone/app/models/usuario.model.dart';
import 'package:evox_interfone/app/pages/auth/login/login_binding.dart';
import 'package:evox_interfone/app/pages/home/home_binding.dart';
import 'package:evox_interfone/app/pages/home/home_view.dart';
import 'package:evox_interfone/app/services/analytics.service.dart';
import 'package:evox_interfone/app/services/notification.service.dart';
//import 'package:connection_notifier/connection_notifier.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:evox_interfone/app/pages/auth/login/login_view.dart';
import 'package:evox_interfone/app/services/data.service.dart';
import 'package:uuid/uuid.dart';

import '../pages/chamada/chamada_view.dart';

class AuthService extends GetxService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final DataService dataSrv = Get.find<DataService>();
  final AnalyticsService analyticsSrv = Get.find<AnalyticsService>();
  final NotificationService notificationSrv = Get.find<NotificationService>();

  RxBool isLoadingLogin = false.obs;
  RxBool isLoadingModalCadastro = false.obs;
  final uuid = const Uuid();
  int vezes = 0;
  String tempPassword = '';
  Future<AuthService> init() async {
    _auth.authStateChanges().listen((User? user) {
      if (user == null) {
        try {
          print('Sem usuario logado');
          Get.off(() => const LoginView(), binding: LoginBinding());
        } catch (e) {
          print(e);
        }
      } else {
        print('usuario de id:' + user.uid + ' esta logado');
        dataSrv.currentUsuario.id = user.uid;
        //   listenToUserChanges();
        dataSrv.userData.snapshots().listen((event) {
          vezes++;
          print('Disparado' + vezes.toString() + 'vezes');
          if (event.exists) {
            dataSrv.currentUsuario = event.data()!;
          }
        });
        dataSrv.userData.get().then((value) {
          analyticsSrv.logUserLogin();
          Get.off(() => const HomeView(), binding: HomeBinding());
        });
      }
    });
    return this;
  }

  void login(String email, String password) async {
    _auth
        .signInWithEmailAndPassword(email: email, password: password)
        .then((value) {
      isLoadingLogin.value = false;
    }).catchError((err) {
      print('Codigo de erro de login');
      print(err.code);
      isLoadingLogin.value = false;
      FirebaseAuthException errorData = err;
      errorHandling(errorData);
    });
    // signOut();
  }

  void signUserUp() async {
    print('CRIANDO CONTA' + tempPassword);
    print(dataSrv.currentUsuario.toJson());
    isLoadingModalCadastro.value = true;
    try {
      final firebaseNewUser = await _auth.createUserWithEmailAndPassword(
          email: dataSrv.currentUsuario.email!, password: tempPassword);
      dataSrv.currentUsuario.id = firebaseNewUser.user!.uid;
      dataSrv.currentUsuario.tokenChamada = uuid.v4();
      dataSrv.currentUsuario.dataCadastro =
          new DateTime.now().millisecondsSinceEpoch;
      dataSrv.currentUsuario.dataAtualizacao =
          new DateTime.now().millisecondsSinceEpoch;
      await firebaseNewUser.user!
          .updateDisplayName(dataSrv.currentUsuario.nome);
      isLoadingModalCadastro.value = false;
      //   debugger();
      dataSrv.saveUserData();
      Get.snackbar('Bem vindo ao Evox', '',
          backgroundColor: Colors.amber, snackPosition: SnackPosition.BOTTOM);
    } catch (onError) {
      FirebaseAuthException? err = onError as FirebaseAuthException?;
      print(err?.message.toString());
      debugger();
      isLoadingModalCadastro.value = false;
      Get.snackbar(
          'Não foi possivel efetuar o cadastro', 'Por favor, tente novamente',
          backgroundColor: Colors.red, snackPosition: SnackPosition.TOP);
    }
  }

/*   DocumentReference<Map<String, dynamic>> getUser(User userData) {
    return _firestore.collection('usuarios').doc(userData.uid);
  } */

  void forgottenPassword(String email) {
    _auth.sendPasswordResetEmail(email: email).then((value) {
      Get.snackbar('Email enviado',
          'Um email foi enviado para o seu email para resetar a sua senha');
    }).catchError((onError) {
      FirebaseAuthException errorData = onError;
      errorData.code;
      print('AQUI ESTA O ERROR =>');
      print(errorData.toString());
      print(errorData.code);
      errorHandling(errorData);
    });
  }

  void signOut() {
    Widget cancelButton = SizedBox(
      width: 500, //MediaQuery.of(Get.context!).size.height * 0.25,
      child: ElevatedButton(
        style: ButtonStyle(
            backgroundColor:
                MaterialStateProperty.all(const Color(0xff252836))),
        child: const Text(
          "Não",
        ),
        onPressed: () {
          Navigator.pop(Get.context!);
          print('Continue por favor lios');
        },
      ),
    );
    Widget continueButton = SizedBox(
      width: 500 /* MediaQuery.of(Get.context!).size.height * 0.25 */,
      child: ElevatedButton(
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.amber)),
        child: const Text(
          "Sair",
          style: TextStyle(color: Color(0xff1F1D2B)),
        ),
        onPressed: () {
          Navigator.pop(Get.context!);
          _auth.signOut().then((value) => print('Profile saiu'));
          print('Continue por favor lisldkskdsklkdlkos');
        },
      ),
    );
    AlertDialog alert = AlertDialog(
      title: const Text(
        "Sair do app?",
        style: TextStyle(
          color: Color(0xff1F1D2B),
        ),
      ),
      content: const Text(
        "Tem certeza de que quer sair?",
        style: TextStyle(
          color: Color(0xff1F1D2B),
        ),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
      backgroundColor: Colors.white,
      actionsAlignment: MainAxisAlignment.start,
      actionsOverflowDirection: VerticalDirection.up,
      //  actionsAlignment: MainAxisAlignment.spaceBetween,
    );

    showDialog(
      context: Get.context!,
      builder: (BuildContext context) {
        return alert;
      },
    );

    //
  }

  void errorHandling(FirebaseAuthException error) {
    switch (error.code) {
      case 'auth/invalid-email':
        Get.snackbar(
          '',
          '',
          titleText: const Text(
            'Email inválido finalizada',
          ),
          messageText: const Text(
            'O email informado não é valido. Por favor, tente novamente ',
          ),
          snackPosition: SnackPosition.TOP,
          backgroundColor: Colors.red,
          duration: const Duration(seconds: 5),
        );
        break;
      case 'wrong-password':
        Get.snackbar(
          '',
          '',
          titleText: const Text(
            'Senha informada não foi encontrada',
          ),
          messageText: const Text(
            'A senha informado se encontrou invalida para o endereço de email informado. Tente novamente com outro email',
          ),
          snackPosition: SnackPosition.TOP,
          backgroundColor: Colors.red,
          duration: const Duration(seconds: 5),
        );
        break;
      case 'auth/user-not-found':
        Get.snackbar(
          '',
          '',
          titleText: const Text(
            'Profile não existe',
          ),
          messageText: const Text(
            'O email informado não pertence a nenhum usuario. ente novamente com outro usuario',
          ),
          snackPosition: SnackPosition.TOP,
          backgroundColor: Colors.red,
          duration: const Duration(seconds: 5),
        );
        break;
      case 'network-request-failed':
        Get.snackbar(
          '',
          '',
          titleText: const Text(
            'Celular sem internet ou com internet falha',
          ),
          messageText: const Text(
            'Não foi possivel efetuar a ação devido a um erro com a sua internet. Tente novamente',
          ),
          snackPosition: SnackPosition.TOP,
          backgroundColor: Colors.red,
          duration: const Duration(seconds: 5),
        );
        break;
      default:
        Get.snackbar(
          '',
          '',
          titleText: const Text(
            'Error',
          ),
          messageText: const Text(
            'Não foi possível concluir sua ação. Tente novamente.',
          ),
          snackPosition: SnackPosition.TOP,
          backgroundColor: Colors.red,
          duration: const Duration(seconds: 5),
        );
    }
  }
}
