import 'dart:async';
import 'package:get/get.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

class AnalyticsService extends GetxService {
  final FirebaseAnalytics analytics = FirebaseAnalytics.instance;
  Future<AnalyticsService> init() async {
    return this;
  }

  logUserLogin() {
    analytics.logLogin();
  }

  logUserActionToGenerateNewQrCode() {
    analytics.logEvent(name: 'pedirNovoQrCode');
  }

  logQtdDemudancasDeDadosDoUsuario() {
    analytics.logEvent(name: 'MudeiDadosDaConta');
  }

  logTempoChamada(int startTime, int endTime) {
    final tempo = endTime - startTime;
    analytics.logEvent(name: 'tempoChamada', parameters: {
      'tempo': tempo,
    });
  }
}
