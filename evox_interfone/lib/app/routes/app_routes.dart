// ignore_for_file: constant_identifier_names

part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const CHAMADA = _Paths.CHAMADA;
  static const QR_CODE = _Paths.QR_CODE;
  static const HISTORICO = _Paths.HISTORICO;
  static const HISTORICO_INFO = _Paths.HISTORICO_INFO;
  static const CONFIG = _Paths.CONFIG;
  static const CADASTRO = _Paths.CADASTRO;
  static const LOGIN = _Paths.LOGIN;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const CHAMADA = '/chamada';
  static const QR_CODE = '/qr-code';
  static const HISTORICO = '/historico';
  static const HISTORICO_INFO = '/historico-info';
  static const CONFIG = '/config';
  static const CADASTRO = '/cadastro';
  static const LOGIN = '/login';
}
