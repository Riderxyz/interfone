
import 'package:get/get.dart';

import '../pages/auth/cadastro/cadastro_binding.dart';
import '../pages/auth/cadastro/cadastro_view.dart';
import '../pages/auth/login/login_binding.dart';
import '../pages/auth/login/login_view.dart';
import '../pages/chamada/chamada_binding.dart';
import '../pages/chamada/chamada_view.dart';
import '../pages/config/config_binding.dart';
import '../pages/config/config_view.dart';
import '../pages/historico-info/historico_info_binding.dart';
import '../pages/historico-info/historico_info_view.dart';
import '../pages/historico/historico_binding.dart';
import '../pages/historico/historico_view.dart';
import '../pages/home/home_binding.dart';
import '../pages/home/home_view.dart';
import '../pages/qrCode/qr_code_binding.dart';
import '../pages/qrCode/qr_code_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  // ignore: constant_identifier_names
  static const INITIAL = Routes.LOGIN;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.CHAMADA,
      page: () => const ChamadaView(),
      binding: ChamadaBinding(),
    ),
     GetPage(
      name: _Paths.QR_CODE,
      page: () => const QrCodeView(),
      binding: QrCodeBinding(),
    ),
    GetPage(
      name: _Paths.HISTORICO,
      page: () => const HistoricoView(),
      binding: HistoricoBinding(),
    ),
    GetPage(
      name: _Paths.HISTORICO_INFO,
      page: () => const HistoricoInfoView(),
      binding: HistoricoInfoBinding(),
    ),
    GetPage(
      name: _Paths.CONFIG,
      page: () => const ConfigView(),
      binding: ConfigBinding(),
    ),
    GetPage(
      name: _Paths.CADASTRO,
      page: () => const CadastroView(),
      binding: CadastroBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => const LoginView(),
      binding: LoginBinding(),
    ),
  ];
}
