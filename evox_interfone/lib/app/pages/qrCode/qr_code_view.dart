import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:evox_interfone/app/services/data.service.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:qr_flutter/qr_flutter.dart';

import './qr_code_controller.dart';

class QrCodeView extends GetView<QrCodeController> {
  const QrCodeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
      DataService dataSrv = Get.find<DataService>();
      RxString qrCodeValue = 'https://visitanteprototipo.netlify.app/#/home/${dataSrv.currentUsuario.tokenChamada!}'.obs;
      QrCodeController controller;
       if (Get.isRegistered<QrCodeController>()) {
      controller = Get.find();
      } else {
      controller = Get.put(QrCodeController());
      }

    return Scaffold(
      appBar: AppBar(
        title: const Text('QrCodeView'),
        centerTitle: true,
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.only(left: 30, right: 30, bottom: 60, top: 30),
          child: Container(
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(20),
              ),
            ),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 30,
                  ),
                  Obx(() => Padding(
                        padding: const EdgeInsets.only(
                            left: 10, right: 10, bottom: 10, top: 10),
                        child: Column(
                          children: [
                            Text(
                              'QRCode de chamada:',
                              style: GoogleFonts.inter(
                                  fontSize: 20, color: Colors.black),
                            ),
                            QrImageView(
                              data: qrCodeValue.value,
                              version: QrVersions.auto,
                              size: controller.getQRcodeSize(),
                            ),
                          ],
                        ),
                      )),
                  const Spacer(),
                  SizedBox(
                    height: 50,
                    width: MediaQuery.of(context).size.width - 100,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.amber),
                      ),
                      onPressed: (() => controller.downloadQRCode()),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const FaIcon(FontAwesomeIcons.download),
                          Text('Baixe e imprima o seu QRCode',
                              textAlign: TextAlign.left,
                              style: GoogleFonts.inter(
                                color: Colors.white,
                                fontSize: 15,
                              )),
                        ],
                      ),
                    ),
                  ),
                  /*     SizedBox(
                    height: 30,
                  ), */
                  SizedBox(
                    height: 50,
                    width: MediaQuery.of(context).size.width - 100,
                    child: OutlinedButton(
                      style: const ButtonStyle(),
                      onPressed: (() => controller.downloadQRCode()),
                      child: Text(
                        'Gerar novo QRCode',
                        textAlign: TextAlign.left,
                        style: GoogleFonts.inter(
                            color: Colors.black,
                            decoration: TextDecoration.underline,
                            fontSize: 16),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
