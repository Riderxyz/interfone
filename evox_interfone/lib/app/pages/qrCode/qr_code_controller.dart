import 'package:evox_interfone/app/services/analytics.service.dart';
import 'package:evox_interfone/app/services/auth.service.dart';
import 'package:evox_interfone/app/services/data.service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:get/get.dart';

class QrCodeController extends GetxController {
  final AuthService authSrv = Get.find<AuthService>();
  final DataService dataSrv = Get.find<DataService>();
  final AnalyticsService analyticsSrv = Get.find<AnalyticsService>();

  RxString teste = ''.obs;
  RxString qrCodeValue = ''.obs;
  RxString barcodeScanRes = ''.obs;
  RxString emCasoDeSemQrCode =
      'Para começar a receber ligações, escaneie o QRCode que você recebeu para cadastra-lo na sua conta'
          .obs;
  @override
  void onInit() {
    qrCodeValue.value =
        'https://visitanteprototipo.netlify.app/#/home/${dataSrv.currentUsuario.tokenChamada!}';
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  double getQRcodeSize() {
    double retorno = 0.0;
    final tamanhoAtual = Get.height;
    if (tamanhoAtual <= 640) {
      retorno = 200.0;
    } else {
      retorno = 250.0;
    }
    return retorno;
  }

  Future<void> scanQR() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes.value = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', 'Cancel', true, ScanMode.QR);
      var indexOfCut = barcodeScanRes.value.indexOf('/home/');
      var userToken = barcodeScanRes.value
          .substring(indexOfCut + 6, barcodeScanRes.value.length);
      print(userToken);
      dataSrv.currentUsuario.tokenChamada = userToken;
      dataSrv.updateUserQRCodeToken(userToken);
      qrCodeValue.value =
          'https://visitanteprototipo.netlify.app/#/home/${dataSrv.currentUsuario.tokenChamada!}';
      print('Isso Funciona!  ${barcodeScanRes.value}');
    } on PlatformException {
      barcodeScanRes.value = 'Failed to get platform version.';
    }
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
  }

  downloadQRCode() {
    analyticsSrv.logUserActionToGenerateNewQrCode();
    Get.snackbar('Ainda em testes', 'disponivel em versões futuras',
        backgroundColor: Colors.amber,
        colorText: Colors.black,
        snackPosition: SnackPosition.BOTTOM);
  }

  @override
  void onClose() {
    super.onClose();
  }
}
