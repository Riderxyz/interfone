import 'package:evox_interfone/app/services/data.service.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import './historico_info_controller.dart';

class HistoricoInfoView extends GetView<HistoricoInfoController> {
  const HistoricoInfoView({Key? key}) : super(key: key);
/*     HistoricoInfoController historicoController =
      Get.put(HistoricoInfoController());  */
  @override
  Widget build(BuildContext context) {
    DataService dataSrv = Get.find<DataService>();
 return Scaffold(
      appBar: AppBar(
        title: Text('Detalhes da chamada',
            style: GoogleFonts.inter(
                color: Colors.black, fontWeight: FontWeight.bold)),
        backgroundColor: Colors.amber,
        centerTitle: false,
      ),
      backgroundColor: const Color(0XFF1f1d2b),
      body: Padding(
        padding: const EdgeInsets.only(right: 15, left: 15, top: 25),
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Nome do Visitante: ',
                  style: TextStyle(fontSize: 20),
                ),
                Text(
                  dataSrv.selectedChamadaForDetail.nome,
                  style: const TextStyle(fontSize: 20),
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Data da ligação: ',
                  style: TextStyle(fontSize: 20),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      DateFormat('dd/MM/yyyy')
                          .format(DateTime.fromMillisecondsSinceEpoch(
                              dataSrv.selectedChamadaForDetail.data))
                          .toString(),
                      style: const TextStyle(fontSize: 20),
                    ),
                    const SizedBox(height: 10),
                    Text(
                      DateFormat('HH:mm')
                          .format(DateTime.fromMillisecondsSinceEpoch(
                              dataSrv.selectedChamadaForDetail.data))
                          .toString(),
                      style: const TextStyle(fontSize: 20),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Tempo de Ligação: ',
                  style: TextStyle(fontSize: 20),
                ),
                Text(
                  DateFormat('mm:ss')
                      .format(DateTime.fromMillisecondsSinceEpoch(dataSrv.selectedChamadaForDetail.tempoChamada))
                      .toString(),
                  style: const TextStyle(fontSize: 20),
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }
}
