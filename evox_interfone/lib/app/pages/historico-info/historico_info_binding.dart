import 'package:get/get.dart';

import './historico_info_controller.dart';

class HistoricoInfoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HistoricoInfoController>(
      () => HistoricoInfoController(),
    );
  }
}
