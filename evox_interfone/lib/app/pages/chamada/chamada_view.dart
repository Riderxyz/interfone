// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:evox_interfone/app/models/chamada.model.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:get/get.dart';

import './chamada_controller.dart';

class ChamadaView extends GetView<ChamadaController> {
  const ChamadaView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
 final FirebaseFirestore _firestore = FirebaseFirestore.instance;
/*      ChamadaController chamadaController = Get.isRegistered<ChamadaController>()
      ? Get.find<ChamadaController>()
      : Get.put(ChamadaController()); */
    return Scaffold(
     backgroundColor: Colors.black,
      body: SafeArea(
        child: StreamBuilder(
          stream: _firestore
              .collection('usuarios')
              .doc(controller.dataSrv.currentUsuario.id)
              .collection('chamadaAtiva')
              .doc('chamadaAtiva')
              .snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
            if (snapshot.hasError) {
              return Center(
                  child: Text('Achei um erro aqui → ${snapshot.error}'));
            }
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CircularProgressIndicator());
            }
            Chamada? chamadaAtual = snapshot.data!.exists
                ? Chamada.fromJson(
                    snapshot.data!.data() as Map<String, dynamic>)
                : null;
            return Obx(() => !controller.chamadaJaComecou.value
                ? const Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                      strokeWidth: 4,
                    ),
                  )
                : Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        // padding: EdgeInsets.all(0),
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                            Radius.circular(30),
                          ),
                        ),
                        child: Center(
                          child: ClipRRect(
                            borderRadius: const BorderRadius.only(
                              bottomLeft: Radius.circular(25.0),
                              bottomRight: Radius.circular(25.0),
                            ),
                            child: Image.network(
                              chamadaAtual!.img,
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height - 300,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                      Center(
                        child: Text(
                          chamadaAtual.nome,
                          style: const TextStyle(
                            fontSize: 30,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Container(
                        decoration: const BoxDecoration(
                          color: Color(0xff252836),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(25.0),
                            topRight: Radius.circular(25.0),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(20),
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Obx(() => ClipRRect(
                                      borderRadius: BorderRadius.circular(120),
                                      child: SizedBox(
                                        width: 80,
                                        height: 80,
                                        child: ElevatedButton(
                                          onPressed: () => controller.mutar(),
                                          style: ButtonStyle(
                                            backgroundColor:
                                                MaterialStateProperty.all<
                                                    Color>(Colors.amber),
                                          ),
                                          child: !controller.isMutado.value
                                              ? const FaIcon(
                                                  FontAwesomeIcons.microphone)
                                              : const FaIcon(FontAwesomeIcons
                                                  .microphoneSlash),
                                        ),
                                      ),
                                    )),
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(120),
                                  child: SizedBox(
                                    width: 80,
                                    height: 80,
                                    child: ElevatedButton(
                                      onPressed: () => controller.desligar(),
                                      style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.red),
                                      ),
                                      child: const FaIcon(FontAwesomeIcons.phone),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ));
          },
        ),
      ),
    );
  }
}
