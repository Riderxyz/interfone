import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:evox_interfone/app/services/data.service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

class ChamadaController extends GetxController {
  String tokenChamada = '';
  final nomeVisitante = ''.obs;
  final data = 0.obs;
  final isMutado = false.obs;
  final mutadoBtnText = 'Mutar'.obs;
  final DataService dataSrv = Get.find<DataService>();
  RxBool chamadaJaComecou = false.obs;
  RxBool _joined = false.obs;
  RxInt? _remoteUid = 0.obs;
  final FirebaseFunctions functions = FirebaseFunctions.instance;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  String _agoraToken = '';
  String _agoraChannelName = '';
  int _agoraUID = 0;
  RtcEngine? agoraEngine = createAgoraRtcEngine();
  final String _appId = 'c292ed23b06c45688833426a3703a278';
  RxBool initializedX = false.obs;
  @override
  void onInit() async {
    _agoraChannelName = dataSrv.currentUsuario.tokenChamada!;
    await [Permission.microphone].request();
    agoraEngine = createAgoraRtcEngine();
    agoraEngine
        ?.initialize(RtcEngineContext(
            appId: _appId,
            channelProfile: ChannelProfileType.channelProfileCommunication1v1))
        .then((value) {
      print('ENGINE CARREGADA!');
      getAgoraToken().then((value) {
        _agoraToken = value.data['token'];
        _agoraUID = value.data['uid'];
        startRTCEventListener();
        initializedX.value = true;
        atender();
      });
    });
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  Future getAgoraToken() async {
    final resToken = await FirebaseFunctions.instance
        .httpsCallable('agoraGenerateRTCToken')
        .call({
      'channel': dataSrv.currentUsuario.tokenChamada,
      'isPublisher': true,
    });

    return resToken;
  }

  startRTCEventListener() {
    agoraEngine?.registerEventHandler(RtcEngineEventHandler(
      onJoinChannelSuccess: (connection, elapsed) async {
        await _firestore
            .collection('usuarios')
            .doc(dataSrv.currentUsuario.id)
            .collection('chamadaAtiva')
            .doc('chamadaAtiva')
            .update({'startTime': DateTime.now().millisecondsSinceEpoch});
     /*    Get.showSnackbar(
          const GetSnackBar(
            title: 'Usuario Local entrou',
            message: 'Usuario Local entrou na chamada',
            icon: Icon(Icons.refresh),
            backgroundColor: Colors.yellow,
            snackPosition: SnackPosition.TOP,
            duration: Duration(seconds: 3),
          ),
        ); */
        _joined.value = true;
      },
      onUserJoined: (RtcConnection connection, int remoteUid, int elapsed) {
        _remoteUid?.value = remoteUid;
        print(connection.channelId);
 /*        Get.showSnackbar(
          GetSnackBar(
            title: 'Visitante entrou',
            message: 'Usuario de id $remoteUid entrou na chamada',
            icon: const Icon(Icons.refresh),
            backgroundColor: Colors.yellow,
            snackPosition: SnackPosition.TOP,
            duration: const Duration(seconds: 3),
          ),
        ); */
      },
      onUserOffline: (RtcConnection connection, int remoteUid,
          UserOfflineReasonType reason) {
        _remoteUid?.value = 0;
        print("Remote user uid:$remoteUid left the channel");
        Get.showSnackbar(
          GetSnackBar(
            title: 'Visitante saiu',
            message: 'Usuario de id $remoteUid saiu da chamada',
            icon: const Icon(Icons.refresh),
            backgroundColor: Colors.yellow,
            snackPosition: SnackPosition.TOP,
            duration: const Duration(seconds: 5),
          ),
        );
        desligar();
      },
      onTokenPrivilegeWillExpire: (connection, token) {
        getAgoraToken().then((value) {
          agoraEngine?.renewToken(value.data['token']);
        });
      },
      onUserMuteAudio: (rtcConectionStatus, remoteUserId, isMuted) {
        print(remoteUserId);
        print(isMuted);
        print(rtcConectionStatus.channelId);
        print(rtcConectionStatus.localUid);
      },
      onLeaveChannel: (RtcConnection connection, RtcStats stats) {
        // cleanChamadaAtual();
      },
    ));
  }

  mutar() async {
    isMutado.value = !isMutado.value;
    agoraEngine?.muteLocalAudioStream(isMutado.value);
  }

  leaveConversation() async {
    return agoraEngine?.leaveChannel();
  }

  atender() async {
    ChannelMediaOptions options = const ChannelMediaOptions(
      clientRoleType: ClientRoleType.clientRoleBroadcaster,
      channelProfile: ChannelProfileType.channelProfileCommunication,
    );
    agoraEngine
        ?.joinChannel(
            token: _agoraToken,
            channelId: _agoraChannelName,
            uid: _agoraUID,
            options: options)
        .then((value) {
      chamadaJaComecou.value = true;
      var t = agoraEngine!.getAudioDeviceManager();
      print(t);
      var audioDevices = t.getPlaybackDevice();
      print(audioDevices);
    }).catchError((onError) {
      print(onError);
    });
  }

  desligar() {
    FlutterCallkitIncoming.endAllCalls().then((value) => null);
    agoraEngine?.leaveChannel().then((value) {
      chamadaJaComecou.value = false;
      //Get.off(() => HomeView());
    });
  }

  @override
  void onClose() {
    super.onClose();
  }
}
