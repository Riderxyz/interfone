// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'dart:ui';
import 'package:flutter/foundation.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:evox_interfone/app/models/chamada.model.dart';
import 'package:evox_interfone/app/models/pushNotificationData.model.dart';
import 'package:evox_interfone/app/services/data.service.dart';
import 'package:get/get.dart';

///
/// A chamada não foi atendida por TIMEOUT;
Future<void> backgroundFunctionOnTimeOut(PushNotificationData pushData) async {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
 
  Chamada userCallTempData = Chamada(
      data: 0,
      endTime: 0,
      startTime: 0,
      img: '',
      isChamadaFinalizada: false,
      nome: '',
      tempoChamada: 0,
      localizacaoLat: 0,
      localizacaoLong: 0);
  var valueCall = await _firestore
      .collection('usuarios')
      .doc(pushData.proprietarioId)
      .collection('chamadaAtiva')
      .doc('chamadaAtiva')
      .get();

  userCallTempData = Chamada.fromJson(valueCall.data() as Map<String, dynamic>);
  if (kDebugMode) {
    print('O nome do maluco q ta ligando é');
  }
  if (kDebugMode) {
    print(userCallTempData.nome);
  }

  userCallTempData.endTime = DateTime.now().millisecondsSinceEpoch;
  int? startTime = userCallTempData.startTime;
  int? endTime = userCallTempData.endTime;
  if (userCallTempData.startTime != null) {
    userCallTempData.tempoChamada = endTime! - startTime!;
  }
  userCallTempData.isChamadaFinalizada = true;
  _firestore
      .collection('usuarios')
      .doc(pushData.proprietarioId)
      .collection('chamadaAtiva')
      .doc('chamadaAtiva')
      .update({'isChamadaFinalizada': true});
  _firestore
      .collection('usuarios')
      .doc(pushData.proprietarioId)
      .collection('historicoChamadas')
      .add({
    'nome': userCallTempData.nome,
    'img': userCallTempData.img,
    'data': userCallTempData.data,
    'tempoChamada': 0,
    'startTime': 0,
    'endTime': 0,
    'isChamadaFinalizada': true,
  });
}
