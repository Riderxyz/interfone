import 'package:evox_interfone/app/models/pushNotificationData.model.dart';
import 'package:evox_interfone/app/pages/chamada/backgroundCall/backgroundCallAcceptFunction.dart';
import 'package:evox_interfone/app/pages/chamada/backgroundCall/backgroundCallDeclineFunction.dart';
import 'package:evox_interfone/app/pages/chamada/backgroundCall/backgroundCallHangUp.dart';
import 'package:evox_interfone/app/pages/chamada/backgroundCall/backgroundCallTimeOutFunction.dart';
import 'package:flutter_callkit_incoming/entities/call_event.dart';

onCallKeepEventReceived(CallEvent? event, PushNotificationData pushData) {
  switch (event!.event) {
    case Event.ACTION_CALL_ACCEPT:
      backgroundFunctionOnAccept(pushData);
      break;
    case Event.ACTION_CALL_DECLINE:
      backgroundFunctionOnDecline(pushData);
      break;
    case Event.ACTION_CALL_ENDED:
      backgroundFunctionOnHangUp(pushData);
      break;
    case Event.ACTION_CALL_TIMEOUT:
      backgroundFunctionOnTimeOut(pushData);
      break;
    default:
      break;
  }
}
