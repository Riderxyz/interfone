import 'package:evox_interfone/app/models/pushNotificationData.model.dart';
import 'package:flutter_callkit_incoming/entities/android_params.dart';
import 'package:flutter_callkit_incoming/entities/call_kit_params.dart';
import 'package:flutter_callkit_incoming/entities/ios_params.dart';
import 'package:uuid/uuid.dart';

CallKitParams callKeepParams(PushNotificationData pushData) {
  final callUUID = const Uuid().v4();
  CallKitParams callKeepParams = CallKitParams(
    id: callUUID,
    nameCaller: pushData.nome,
    appName: 'Evox ',
    avatar: pushData.visitorsPhoto,
    handle: '0123456789',
    type: 0,
    duration: 60000,
    textAccept: 'Aceitar',
    textDecline: 'Recusar',
    extra: <String, dynamic>{'userId': '1a2b3c4d'},
    headers: <String, dynamic>{'apiKey': 'Abc@123!', 'platform': 'flutter'},
    android: AndroidParams(
      isCustomNotification: true,
      isShowLogo: true,
      /* isShowCallback: false,
      isShowMissedCallNotification: true, */
      ringtonePath: 'officephonering',
      backgroundColor: '#333333',
      backgroundUrl: pushData.visitorsPhoto,
      actionColor: '#4CAF50',
    ),
    ios: IOSParams(
      iconName: 'CallKitLogo',
      handleType: '',
      supportsVideo: true,
      maximumCallGroups: 2,
      maximumCallsPerCallGroup: 1,
      audioSessionMode: 'default',
      audioSessionActive: true,
      audioSessionPreferredSampleRate: 44100.0,
      audioSessionPreferredIOBufferDuration: 0.005,
      supportsDTMF: true,
      supportsHolding: true,
      supportsGrouping: false,
      supportsUngrouping: false,
      ringtonePath: 'system_ringtone_default',
    ),
  );
  return callKeepParams;
}
