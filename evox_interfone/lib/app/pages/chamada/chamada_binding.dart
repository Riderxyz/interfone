import 'package:get/get.dart';

import './chamada_controller.dart';

class ChamadaBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ChamadaController>(
      () => ChamadaController(),
    );
  }
}
