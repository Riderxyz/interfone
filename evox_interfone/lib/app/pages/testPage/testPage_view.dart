import 'package:evox_interfone/app/components/sidebarMenu/side_bar_menu_view.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import './testPage_controller.dart';

class TestPageView extends GetView<TestPageController> {
  const TestPageView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('TestPageView'),
        centerTitle: true,
      ),
      drawer: SideBarMenuView(),
      body: Column(
        children: [
          const Center(
            child: Text(
              'HomeView is working for now...',
              style: TextStyle(fontSize: 20),
            ),
          ),
          SizedBox(
            width: 160,
            height: 120,
            child: ElevatedButton(
                onPressed: (() => controller.createNewNotification()),
                child: const Text('GO For It! ! !')),
          ),
          const SizedBox(
            height: 150,
          ),
          Column(
            children: [
              ElevatedButton(
                  onPressed: (() => controller.test('officePhoneRing')),
                  child: const Text('TestNotificattion 1')),
              ElevatedButton(
                  onPressed: (() => controller.test('ringing')),
                  style: const ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(Colors.amber)),
                  child: const Text('TestNotificattion 2')),
              ElevatedButton(
                  onPressed: (() => controller.test('ringtone')),
                  style: const ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(Colors.purple)),
                  child: const Text('TestNotificattion 3')),
            ],
          )
        ],
      ),
    );
  }
}
