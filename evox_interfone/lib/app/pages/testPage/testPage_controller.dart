import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';
import 'package:evox_interfone/app/pages/testPage/callKeepConfigForTesting.dart';
import 'package:flutter/material.dart';
import 'package:flutter_callkit_incoming/flutter_callkit_incoming.dart';
import 'package:get/get.dart';
import 'package:flutter/foundation.dart';

class TestPageController extends GetxController {
  //TODO: Implement HomeController
  final db = FirebaseFirestore.instance;

  final Dio dio = Dio();
  late GetConnect client;
  @override
  void onInit() {
    client = GetConnect();
    super.onInit();
  }

  test(String ringToneName) async {
    final params = callKeepParamsForTesting(ringToneName);
    FlutterCallkitIncoming.showCallkitIncoming(params).then((value) {
      if (kDebugMode) {
        print('ABRINDO CALLKIT');
      }
      if (kDebugMode) {
        print(value);
      }
    }).onError((error, stackTrace) {
      if (kDebugMode) {
        print(error);
      }
    });
    //  FlutterCallkitIncoming.showMissCallNotification(params);
    var calls = await FlutterCallkitIncoming.activeCalls();
    if (kDebugMode) {
      print('ESTOU AQUI!');
    }
    if (kDebugMode) {
      print(calls);
    }
    await db.collection("usuarios").get().then((event) {
      for (var doc in event.docs) {
        //  print("${doc.id} => ${doc.data()['email']}");
      }
    });
final response = await client.get('https://viacep.com.br/ws/21550020/json/');
if (response.statusCode == 200) {
      final users = await response.body();
      if (kDebugMode) {
        print(users);
      }
  
}
  }

  Future<void> createNewNotification() async {
    bool isAllowed = await AwesomeNotifications().isNotificationAllowed();
    if (!isAllowed) isAllowed = await displayNotificationRationale();
    if (!isAllowed) return;

    await AwesomeNotifications().createNotification(
        content: NotificationContent(
            id: -1, // -1 is replaced by a random number
            channelKey: 'alerts',
            title: 'Huston! The eagle has landed!',
            body:
                "A small step for a man, but a giant leap to Flutter's community!",
            bigPicture: 'https://storage.googleapis.com/cms-storage-bucket/d406c736e7c4c57f5f61.png',
            largeIcon: 'https://storage.googleapis.com/cms-storage-bucket/0dbfcc7a59cd1cf16282.png',
            locked: true,
            notificationLayout: NotificationLayout.BigPicture,
            category: NotificationCategory.Call,
            payload: {'notificationId': '1234567890'}),
        actionButtons: [
          NotificationActionButton(key: 'REDIRECT', label: 'Redirect'),
          NotificationActionButton(
              key: 'DISMISS',
              label: 'Dismiss',
              actionType: ActionType.DismissAction,
              isDangerousOption: true)
        ]);
  }

  static Future<bool> displayNotificationRationale() async {
    bool userAuthorized = false;
    BuildContext context = Get.context!;
    await showDialog(
        context: context,
        builder: (BuildContext ctx) {
          return AlertDialog(
            title: Text('Get Notified!',
                style: Theme.of(context).textTheme.titleLarge),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Image.asset(
                        'assets/images/animated-bell.gif',
                        height: MediaQuery.of(context).size.height * 0.3,
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                const Text(
                    'Allow Awesome Notifications to send you beautiful notifications!'),
              ],
            ),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(ctx).pop();
                  },
                  child: Text(
                    'Deny',
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge
                        ?.copyWith(color: Colors.red),
                  )),
              TextButton(
                  onPressed: () async {
                    userAuthorized = true;
                    Navigator.of(ctx).pop();
                  },
                  child: Text(
                    'Allow',
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge
                        ?.copyWith(color: Colors.deepPurple),
                  )),
            ],
          );
        });
    return userAuthorized &&
        await AwesomeNotifications().requestPermissionToSendNotifications();
  }
}
