import 'package:get/get.dart';

import './testPage_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TestPageController>(
      () => TestPageController(),
    );
  }
}
