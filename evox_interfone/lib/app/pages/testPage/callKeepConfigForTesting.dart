// ignore: file_names
import 'package:uuid/uuid.dart';
import 'package:flutter_callkit_incoming/entities/entities.dart';

CallKitParams callKeepParamsForTesting(String ringToneName) {
  final callUUID = const Uuid().v4();
  late CallKitParams callKeepParams;
  switch (ringToneName) {
    case 'officePhoneRing':
      callKeepParams = CallKitParams(
        id: callUUID,
        nameCaller: 'pushData.nome',
        appName: 'Evox ',
        avatar: 'pushData.visitorsPhoto',
        handle: '0123456789',
        type: 0,
        duration: 60000,
        textAccept: 'Aceitar',
        textDecline: 'Recusar',
        //textMissedCall: 'Missed call',
        //textCallback: 'Call back',
        extra: <String, dynamic>{'userId': '1a2b3c4d'},
        headers: <String, dynamic>{'apiKey': 'Abc@123!', 'platform': 'flutter'},
        android: const AndroidParams(
          isCustomNotification: true,
          isShowLogo: true,
          /* isShowCallback: false,
      isShowMissedCallNotification: true, */
          ringtonePath: 'officephonering',
          backgroundColor: '#333333',
          backgroundUrl: 'https://i.pravatar.cc/500',
          actionColor: '#4CAF50',
        ),
        ios: IOSParams(
          iconName: 'CallKitLogo',
          handleType: '',
          supportsVideo: true,
          maximumCallGroups: 2,
          maximumCallsPerCallGroup: 1,
          audioSessionMode: 'default',
          audioSessionActive: true,
          audioSessionPreferredSampleRate: 44100.0,
          audioSessionPreferredIOBufferDuration: 0.005,
          supportsDTMF: true,
          supportsHolding: true,
          supportsGrouping: false,
          supportsUngrouping: false,
          ringtonePath: 'system_ringtone_default',
        ),
      );

    case 'ringing':
      callKeepParams = CallKitParams(
        id: callUUID,
        nameCaller: 'pushData.nome',
        appName: 'Evox ',
        avatar: 'pushData.visitorsPhoto',
        handle: '0123456789',
        type: 0,
        duration: 60000,
        textAccept: 'Aceitar',
        textDecline: 'Recusar',
        //textMissedCall: 'Missed call',
        //textCallback: 'Call back',
        extra: <String, dynamic>{'userId': '1a2b3c4d'},
        headers: <String, dynamic>{'apiKey': 'Abc@123!', 'platform': 'flutter'},
        android: const AndroidParams(
          isCustomNotification: true,
          isShowLogo: true,
          /* isShowCallback: false,
      isShowMissedCallNotification: true, */
          ringtonePath: 'ringtone',
          backgroundColor: '#333333',
          backgroundUrl: 'https://i.pravatar.cc/500',
          actionColor: '#4CAF50',
        ),
        ios: IOSParams(
          iconName: 'CallKitLogo',
          handleType: '',
          supportsVideo: true,
          maximumCallGroups: 2,
          maximumCallsPerCallGroup: 1,
          audioSessionMode: 'default',
          audioSessionActive: true,
          audioSessionPreferredSampleRate: 44100.0,
          audioSessionPreferredIOBufferDuration: 0.005,
          supportsDTMF: true,
          supportsHolding: true,
          supportsGrouping: false,
          supportsUngrouping: false,
          ringtonePath: 'system_ringtone_default',
        ),
      );

    case 'ringtone':
      callKeepParams = CallKitParams(
        id: callUUID,
        nameCaller: 'pushData.nome',
        appName: 'Evox ',
        avatar: 'pushData.visitorsPhoto',
        handle: '0123456789',
        type: 0,
        duration: 60000,
        textAccept: 'Aceitar',
        textDecline: 'Recusar',
        //textMissedCall: 'Missed call',
        //textCallback: 'Call back',
        extra: <String, dynamic>{'userId': '1a2b3c4d'},
        headers: <String, dynamic>{'apiKey': 'Abc@123!', 'platform': 'flutter'},
        android: const AndroidParams(
          isCustomNotification: true,
          isShowLogo: true,
          /* isShowCallback: false,
      isShowMissedCallNotification: true, */
          ringtonePath: 'ringtonex',
          backgroundColor: '#333333',
          backgroundUrl: 'https://i.pravatar.cc/500',
          actionColor: '#4CAF50',
        ),
        ios: IOSParams(
          iconName: 'CallKitLogo',
          handleType: '',
          supportsVideo: true,
          maximumCallGroups: 2,
          maximumCallsPerCallGroup: 1,
          audioSessionMode: 'default',
          audioSessionActive: true,
          audioSessionPreferredSampleRate: 44100.0,
          audioSessionPreferredIOBufferDuration: 0.005,
          supportsDTMF: true,
          supportsHolding: true,
          supportsGrouping: false,
          supportsUngrouping: false,
          ringtonePath: 'system_ringtone_default',
        ),
      );

      break;
    default:
  }

  return callKeepParams;
}
