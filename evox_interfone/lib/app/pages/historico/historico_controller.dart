import 'package:evox_interfone/app/services/data.service.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class HistoricoController extends GetxController {
  //TODO: Implement HistoricoController

  final DataService dataSrv = Get.find<DataService>();

  var list = [];
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  String formatTimeInChamadas(int timestamp) {
    String time = '';
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(timestamp);
    time = DateFormat('dd/MM/yyyy').format(dateTime);
    // print('AQUI ESTA VOCÊ' + timestamp.toString());
    return time;
  }

  String formatHourInChamadas(int timestamp) {
    var hours = DateFormat('HH:mm');
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(timestamp);
    return hours.format(dateTime);
  }

  goToDetails(Function openContainer, chamadaInfo) {
    dataSrv.selectedChamadaForDetail = chamadaInfo;
    openContainer();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
