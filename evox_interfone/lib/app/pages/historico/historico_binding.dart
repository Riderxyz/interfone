import 'package:get/get.dart';

import './historico_controller.dart';

class HistoricoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HistoricoController>(
      () => HistoricoController(),
    );
  }
}
