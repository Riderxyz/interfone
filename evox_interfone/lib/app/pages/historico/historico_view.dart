import 'package:animations/animations.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:evox_interfone/app/components/sidebarMenu/side_bar_menu_view.dart';
import 'package:evox_interfone/app/models/chamada.model.dart';
import 'package:evox_interfone/app/pages/historico-info/historico_info_view.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import './historico_controller.dart';

class HistoricoView extends GetView<HistoricoController> {
  const HistoricoView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Evoxcall',
            style: GoogleFonts.inter(
                color: Colors.black, fontWeight: FontWeight.bold)),
        backgroundColor: Colors.amber,
        centerTitle: false,
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      drawer: SideBarMenuView(),
    body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, bottom: 0, top: 40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Boas vindas ${controller.dataSrv.currentUsuario.nome}',
                textAlign: TextAlign.start,
                style: GoogleFonts.inter(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                  color: const Color(0xFFFFC61A),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                'Histórico de chamadas',
                textAlign: TextAlign.start,
                style: GoogleFonts.inter(
                  fontSize: 16,
                  color: Colors.white,
                ),
              ),
              const SizedBox(height: 30),
              StreamBuilder(
                  stream: controller.dataSrv.getUltimasChamada
                      .orderBy('data', descending: true)
                      /* .limitToLast(10) */
                      .snapshots(), //FirebaseFirestore.instance.collection('usuarios').snapshots(),
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (snapshot.hasError) {
                      return Center(
                          child:
                              Text('Achei um erro aqui → ${snapshot.error}'));
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const Center(child: CircularProgressIndicator());
                    }
                    /* if (controller.list.length == 0) {
                    return Center(child: Text('Histórico vazio'));
                  } */

                    controller.list = snapshot.data!.docs;
                    return Expanded(
                        child: ListView.builder(
                            itemCount: controller.list.length,
                            shrinkWrap: true,
                            itemBuilder: (BuildContext context, int i) {
                              Chamada? userData =
                                  snapshot.data!.docs[i].data() as Chamada?;
                              return Padding(
                                padding: const EdgeInsets.only(bottom: 20),
                                child: Container(
                                  decoration: const BoxDecoration(
                                    color: Color(0xFF252836),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(20),
                                    ),
                                  ),
                                  child: OpenContainer(
                                    middleColor: Colors.amber,
                                    openElevation: 10,
                                    closedElevation: 15,
                                    closedShape: const RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(10)),
                                    ),
                                    closedColor: const Color(0xFF252836),
                                    transitionType:
                                        ContainerTransitionType.fade,
                                    transitionDuration:
                                        const Duration(milliseconds: 900),
                                    openBuilder: (context, _) =>
                                        const HistoricoInfoView(),
                                    closedBuilder: (context, openContainer) =>
                                        SizedBox(
                                      height: 85,
                                      child: Center(
                                        child: ListTile(
                                          onTap: (() => controller.goToDetails(
                                              openContainer, userData)),
                                          title: Text(
                                            userData!.nome.toString(),
                                            style: GoogleFonts.inter(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20,
                                                color: Colors.white),
                                          ),
                                          subtitle: Text(
                                            'Data da ligação: ${controller.formatTimeInChamadas(userData.data)}',
                                            style: GoogleFonts.inter(
                                                fontSize: 15,
                                                color: Colors.grey),
                                          ),
                                          leading: SizedBox(
                                            height: 50,
                                            width: 50,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  color: const Color(0xFF2F3142),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: const Center(
                                                child: FaIcon(
                                                  FontAwesomeIcons.userLarge,
                                                  color: Colors.white,
                                                  size: 15,
                                                ),
                                              ),
                                            ),
                                          ),
                                          selected: false,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            }));
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
