import 'package:animations/animations.dart';
import 'package:evox_interfone/app/components/sidebarMenu/side_bar_menu_view.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import './config_controller.dart';

class ConfigView extends GetView<ConfigController> {
  const ConfigView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0XFF1f1d2b),
      appBar: AppBar(
        title: Text(
          'Configurações',
          style: GoogleFonts.inter(
              fontWeight: FontWeight.bold,
              //fontSize: 20,
              color: Colors.black),
        ),
        backgroundColor: Colors.amber,
        iconTheme: const IconThemeData(color: Colors.black),
        centerTitle: true,
      ),
      drawer: SideBarMenuView(),
      body: Padding(
        padding:
            const EdgeInsets.only(left: 20, right: 20, bottom: 60, top: 20),
        child: ListView.builder(
          itemCount: controller.configItem.length,
          itemBuilder: ((itemBuilder, i) {
            return Padding(
              padding: const EdgeInsets.only(bottom: 15),
              child: OpenContainer(
                middleColor: Colors.amber,
                openElevation: 10,
                closedElevation: 15,
                closedShape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                closedColor: const Color(0xFF252836),
                transitionType: ContainerTransitionType.fade,
                transitionDuration: const Duration(milliseconds: 900),
                openBuilder: (context, _) => controller
                    .pageDestination(controller.configItem[i]['action']),
                closedBuilder: (context, openContainer) => Container(
                  decoration: const BoxDecoration(
                    color: Color(0xFF252836),
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                  ),
                  child: SizedBox(
                    height: 85,
                    child: Center(
                      child: ListTile(
                        title: Text(
                          controller.configItem[i]['title'].toString(),
                          style: GoogleFonts.inter(
                              fontWeight: FontWeight.bold,
                              //fontSize: 20,
                              color: Colors.white),
                        ),
                        subtitle: Text(
                          controller.configItem[i]['subtitle'].toString(),
                          style: GoogleFonts.inter(
                              fontWeight: FontWeight.normal,
                              // fontSize: 15,
                              color: Colors.white),
                        ),
                        leading: SizedBox(
                          height: 50,
                          width: 50,
                          child: Container(
                            decoration: BoxDecoration(
                                color: const Color(0xFF2F3142),
                                borderRadius: BorderRadius.circular(10)),
                            child: Center(
                              child: FaIcon(
                                controller.configItem[i]['icon'],
                                size: 20,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                        enableFeedback: true,
                        onTap: (() => controller.doAction(
                                controller.configItem[i]['action'],
                                openContainer) // controller.goTo(controller.configItem[i]['path'].toString())),
                            ),
                      ),
                    ),
                  ),
                ),
              ),
            );
          }),
        ),
      ),
    );
  }
}
