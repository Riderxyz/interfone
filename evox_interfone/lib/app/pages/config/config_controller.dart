// ignore_for_file: avoid_print

import 'package:evox_interfone/app/enviroment.dart';
import 'package:evox_interfone/app/pages/qrCode/qr_code_view.dart';
import 'package:evox_interfone/app/services/auth.service.dart';
import 'package:evox_interfone/app/services/data.service.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

class ConfigController extends GetxController {
    final AuthService authSrv = Get.find<AuthService>();
  final DataService dataSrv = Get.find<DataService>();
  RxString teste = ''.obs;
  RxString qrCodeValue = ''.obs;
  RxString barcodeScanRes = ''.obs;
  RxString emCasoDeSemQrCode =
      'Para começar a receber ligações, escaneie o QRCode que você recebeu para cadastra-lo na sua conta'
          .obs;


 final List<Map<String, dynamic>> configItem = [
    /*    {
      'title': 'Editar conta',
      'subtitle': 'Trocar senha, deletar e criar perfis e usuario',
      'onlyAdmin': true,
      'icon': FontAwesomeIcons.solidStar,
      'action': ConfigViewListTypes.editAccount
    }, */
/*     {
      'title': 'Escolher o perfil ativo',
      'subtitle': 'Escolha o perfil que ficará ativo no celular',
      'onlyAdmin': false,
      'icon': FontAwesomeIcons.person,
      'action': ConfigViewListTypes.chooseProfile
    }, */
    {
      'title': 'Editar perfil',
      'subtitle':
          'Editar o perfil de usuario atual(Nome, celular, Trocar senha, etc...)',
      'onlyAdmin': false,
      'icon': FontAwesomeIcons.penToSquare,
      'action': ConfigViewListTypes.editProfile
    },
    {
      'title': 'QrCode',
      'subtitle': 'Visualizar o QrCode e trocar o mesmo caso deseje',
      'onlyAdmin': false,
      'icon': FontAwesomeIcons.penToSquare,
      'action': ConfigViewListTypes.qrCode
    },
    {
      'title': 'Sair',
      'subtitle':
          'Deslogar do aplicativo. Apaga todos os dados salvos localmente',
      'onlyAdmin': false,
      'icon': FontAwesomeIcons.rightFromBracket,
      'action': ConfigViewListTypes.exitApp
    }
  ];
   @override
  void onInit() {
    qrCodeValue.value = 'https://visitanteprototipo.netlify.app/#/home/${dataSrv.currentUsuario.tokenChamada!}';
    print('linha 20 ${qrCodeValue.value}');
    print(dataSrv.currentUsuario.tokenChamada!);
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  
  pageDestination(ConfigViewListTypes action) {
    var retorno = null;
    switch (action) {
      case ConfigViewListTypes.editProfile:
        retorno = const QrCodeView();
        break;
      case ConfigViewListTypes.qrCode:
        retorno = const QrCodeView();
        break;
      case ConfigViewListTypes.exitApp:
        retorno = const QrCodeView();
        break;
      case ConfigViewListTypes.automation:
        print('sem função por enquanto');
        break;
      case ConfigViewListTypes.editAccount:
        print('sem função por enquanto');
        break;
    }
    return retorno;
  }

   doAction(ConfigViewListTypes action, Function openContainer) {
    switch (action) {
      case ConfigViewListTypes.editProfile:
        Get.snackbar('Ainda em testes', 'disponivel em versões futuras',
            backgroundColor: Colors.amber,
            colorText: Colors.black,
            snackPosition: SnackPosition.BOTTOM);
        print('teste 12');
        break;
      case ConfigViewListTypes.qrCode:
        openContainer();
        print('teste 13');

        break;
      case ConfigViewListTypes.exitApp:
        print('teste 14');
        authSrv.signOut();
        break;
      case ConfigViewListTypes.automation:
        print('sem função por enquanto');
        break;
      case ConfigViewListTypes.editAccount:
        Get.snackbar('Ainda em testes', 'disponivel em versões futuras',
            backgroundColor: Colors.amber,
            colorText: Colors.black,
            snackPosition: SnackPosition.BOTTOM);
        break;
    }
  }


  @override
  void onClose() {
    super.onClose();
  }
}
