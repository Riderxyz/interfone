import 'dart:isolate';
import 'dart:ui';

import 'package:evox_interfone/app/models/chamada.model.dart';
import 'package:evox_interfone/app/pages/chamada/chamada_binding.dart';
import 'package:evox_interfone/app/pages/chamada/chamada_view.dart';
import 'package:evox_interfone/app/services/data.service.dart';
import 'package:evox_interfone/app/services/notification.service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class HomeController extends GetxController {
  final DataService dataSrv = Get.find<DataService>();
  final NotificationService notificationSrv = Get.find<NotificationService>();
  final nome = ''.obs;
  var list = [];
  late GetConnect client;
  final ReceivePort _portForCall = ReceivePort();
  @override
  void onInit() {
    client = GetConnect();
    notificationSrv.fcmToken(dataSrv.currentUsuario.id!);
    IsolateNameServer.removePortNameMapping('portForAgoraIoCall');
    IsolateNameServer.registerPortWithName(
        _portForCall.sendPort, 'portForAgoraIoCall');
    goToChamadaPageWhenComingFromBackdround();
    super.onInit();
  }

  String formatTimeInChamadas(int timestamp) {
    String time = '';
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(timestamp);
    time = DateFormat('dd/MM/yyyy HH:mm').format(dateTime);
    return time;
  }

  set selectedChamadaForDetail(Chamada chamada) {
    dataSrv.selectedChamadaForDetail = chamada;
  }

  @override
  void onReady() {
    super.onReady();
  }

  test() async {
  }

  goToChamadaPageWhenComingFromBackdround() {
    _portForCall.listen((message) {
     Get.to(() => const ChamadaView(),
         binding: ChamadaBinding(),
            transition: Transition.upToDown,
            duration: const Duration(milliseconds: 500),
            curve: Curves.easeIn);  
    });
  }

  @override
  void onClose() {
    super.onClose();
  }
}
