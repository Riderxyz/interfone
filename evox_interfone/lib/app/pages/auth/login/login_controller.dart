import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:evox_interfone/app/services/auth.service.dart';
import 'package:evox_interfone/app/services/data.service.dart';
class LoginController extends GetxController {
  //TODO: Implement LoginController
  final AuthService authSrv = Get.find<AuthService>();
  final DataService dataSrv = Get.find<DataService>();
  

    GlobalKey<FormState> _loginFormKey = GlobalKey<FormState>();
  late TextEditingController emailController, passwordController;
  var email = '';
  var password = '';
  RxBool passwordVisible = false.obs;
  final width = 500.obs;
  final height = 50.obs;
  final icon = 20.obs;

  GlobalKey<FormState> get loginFormKey => _loginFormKey;

  set loginFormKey(GlobalKey<FormState> value) {
    _loginFormKey = value;
  }
  @override
  void onInit() {
     emailController = TextEditingController();
    passwordController = TextEditingController();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  String? validateEmail(String value) {
    String? retorno;
    if (!GetUtils.isEmail(value)) {
      retorno = 'Email inválido';
    }
    return retorno;
  }

  String? validatePassword(String value) {
    String? retorno;
    if (value.length < 6) {
      retorno = 'Senha precisa ter mais de 6 caracteres';
    }
    return retorno;
  }

   bool checkLogin() {
    final retorno = _loginFormKey.currentState!.validate();
    return retorno;
  }

  doLogin() {
    if (checkLogin()) {
      _loginFormKey.currentState!.save();
      authSrv.isLoadingLogin.value = true;
      authSrv.login(email, password);
    } else {
      return;
    }
  }

  @override
  void onClose() {
    emailController.dispose();
    passwordController.dispose();
    super.onClose();
  }

}
