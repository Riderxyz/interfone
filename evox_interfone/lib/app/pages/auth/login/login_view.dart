import 'dart:async';

import 'package:evox_interfone/app/pages/auth/cadastro/cadastro_binding.dart';
import 'package:flutter/material.dart';
import 'package:evox_interfone/app/components/modal/modalEsqueciSenha/modal_esqueci_senha_view.dart';
import 'package:evox_interfone/app/pages/auth/cadastro/cadastro_view.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import './login_controller.dart';

class LoginView extends GetView<LoginController> {
  const LoginView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView(
          children: [
            Image.asset(
              'assets/images/logo.png',
              width: 250,
              height: 250,
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 10, bottom: 0, left: 30, right: 30),
              child: SingleChildScrollView(
                child: Obx(
                  () => Form(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    key: controller.loginFormKey,
                    child: controller.authSrv.isLoadingLogin.value
                        ? const Center(
                            child: SizedBox(
                              width: 250,
                              height: 250,
                              child: CircularProgressIndicator(
                                  strokeWidth: 8,
                                  valueColor:
                                      AlwaysStoppedAnimation(Colors.black)),
                            ),
                          )
                        : Column(
                            // mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              const SizedBox(
                                height: 20,
                              ),
                              TextFormField(
                                decoration: const InputDecoration(
                                  labelText: 'E-mail',
                                  labelStyle: TextStyle(color: Colors.amber),
                                  prefixIcon: Icon(
                                    Icons.email_outlined,
                                    color: Colors.amber,
                                  ),
                                ),
                                style: GoogleFonts.inter(color: Colors.white),
                                keyboardType: TextInputType.emailAddress,
                                controller: controller.emailController,
                                onSaved: ((value) => controller.email = value!),
                                validator: ((value) {
                                  return controller.validateEmail(value!);
                                }),
                              ),
                              const SizedBox(
                                height: 21,
                              ),
                              TextFormField(
                                decoration: InputDecoration(
                                  labelText: 'Senha',
                                  labelStyle:
                                      const TextStyle(color: Colors.amber),
                                  prefixIcon: const Icon(
                                    Icons.lock_outline,
                                    color: Colors.amber,
                                  ),
                                  suffixIcon: IconButton(
                                    icon: Icon(
                                        // Based on passwordVisible state choose the icon
                                        controller.passwordVisible.value
                                            ? Icons.visibility
                                            : Icons.visibility_off,
                                        color: Colors.amber),
                                    onPressed: () {
                                      controller.passwordVisible.value =
                                          !controller.passwordVisible.value;
                                    },
                                  ),
                                ),
                                style: const TextStyle(color: Colors.white),
                                obscureText: !controller.passwordVisible.value,
                                keyboardType: TextInputType.visiblePassword,
                                controller: controller.passwordController,
                                onSaved: ((value) =>
                                    controller.password = value!),
                                validator: ((value) {
                                  return controller.validatePassword(value!);
                                }),
                              ),
                              TextButton(
                                onPressed: (() => showModalBottomSheet(
                                      context: context,
                                      isScrollControlled: true,
                                      backgroundColor: Colors.transparent,
                                      builder: (context) => Container(
                                        child: ModalEsqueciSenhaView(),
                                      ),
                                    )),
                                child: const Text(
                                  'Esqueci minha senha',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                              const SizedBox(
                                height: 30,
                              ),
                              SizedBox(
                                width: 300,
                                height: 37,
                                child: ElevatedButton(
                                  onPressed: (() => controller.doLogin()),
                                  style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.amber),
                                  ),
                                  child: Text(
                                    'Login',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.grey.shade800,
                                        fontSize: 20),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              SizedBox(
                                width: 300,
                                height: 37,
                                child: ElevatedButton(
                                  onPressed: (() => Timer(
                                      const Duration(seconds: 1),
                                      () => Get.to(() => const CadastroView(),
                                      binding: CadastroBinding(),
                                          transition: Transition.leftToRight))),
                                  style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.black54),
                                  ),
                                  child: const Text(
                                    'Fazer Cadastro',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 20),
                                  ),
                                ),
                              )
                            ],
                          ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
