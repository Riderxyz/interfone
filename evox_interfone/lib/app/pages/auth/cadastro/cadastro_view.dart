import 'package:easy_debounce/easy_debounce.dart';
import 'package:easy_mask/easy_mask.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import './cadastro_controller.dart';

class CadastroView extends GetView<CadastroController> {
  const CadastroView({Key? key}) : super(key: key);
    /* final CadastroController controller = Get.put(CadastroController()); */
  @override
  Widget build(BuildContext context) {
     return Scaffold(
      appBar: AppBar(
        title: Text('Cadastro de usuario', 
         style: GoogleFonts.inter()),
        backgroundColor: Colors.amber,
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Obx(
          () => controller.authSrv.isLoadingModalCadastro.value
              ? Column(
                  children: [
                    const SizedBox(
                      height: 150,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 15, left: 15),
                      child: SizedBox(
                        height: 300,
                        width: MediaQuery.of(context).size.width,
                        child: const CircularProgressIndicator(
                          strokeWidth: 10,
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    Title(
                        color: Colors.black,
                        child: Text(
                          'Carregando',
                          style: GoogleFonts.inter(
                              fontSize: 30, color: Colors.black),
                        ))
                  ],
                )
              : Form(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  key: controller.cadastroFormKey,
                  child: ListView(
                    /* controller: scrollController, */
                    shrinkWrap: true,
                    children: [
                      const SizedBox(
                        height: 16,
                      ),
                      Title(
                        color: Colors.white,
                        child: Text(
                          'Dados Básicos',
                          style: GoogleFonts.inter(
                            fontSize: 30,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelStyle: GoogleFonts.inter(
                            color: Colors.white,
                          ),
                          labelText: 'Nome completo',
                          prefixIcon: const Icon(
                            Icons.person,
                            color: Colors.amber,
                          ),
                        ),
                        style: GoogleFonts.inter(color: Colors.white),
                        autofocus: false,
                        keyboardType: TextInputType.name,
                        controller: controller.nomeController,
                        onSaved: ((value) {
                          // print('salvei Nome' + value!);
                          controller.dataSrv.currentUsuario.nome = value!;
                        }),
                        validator: ((value) {
                          controller.dataSrv.currentUsuario.nome = value;
                          return controller.validateName(value!);
                        }),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelStyle: GoogleFonts.inter(
                            color: Colors.white,
                          ),
                          labelText: 'Celular',
                          prefixIcon: const Icon(
                            Icons.phone,
                            color: Colors.amber,
                          ),
                        ),
                        style: GoogleFonts.inter(color: Colors.white),
                        keyboardType: TextInputType.phone,
                        inputFormatters: [
                          TextInputMask(
                              mask: '(99) 99999-9999', placeholder: '_')
                        ],
                        controller: controller.phoneController,
                        onSaved: ((value) =>
                            controller.dataSrv.currentUsuario.celular = value!),
                        validator: ((value) {
                          return controller.validatePhone(value!);
                        }),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelStyle: GoogleFonts.inter(
                            color: Colors.white,
                          ),
                          labelText: 'E-mail',
                          prefixIcon: const Icon(
                            Icons.mail,
                            color: Colors.amber,
                          ),
                        ),
                        style: GoogleFonts.inter(color: Colors.white),
                        keyboardType: TextInputType.emailAddress,
                        controller: controller.emailController,
                        onSaved: ((value) =>
                            controller.dataSrv.currentUsuario.email = value!),
                        validator: ((value) {
                          return controller.validateEmail(value!);
                        }),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelStyle: GoogleFonts.inter(
                            color: Colors.white,
                          ),
                          labelText: 'Senha',
                          prefixIcon: const Icon(
                            Icons.lock,
                            color: Colors.amber,
                          ),
                        ),
                        style: GoogleFonts.inter(color: Colors.white),
                        obscureText: true,
                        keyboardType: TextInputType.visiblePassword,
                        controller: controller.passwordController,
                        onSaved: ((value) =>
                            controller.authSrv.tempPassword = value!),
                        validator: ((value) {
                          print(value);
                          controller.authSrv.tempPassword = value!;
                          return controller.validatePassword(value);
                        }),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Title(
                        color: Colors.black,
                        child: Text(
                          'Endereço',
                          style: GoogleFonts.inter(
                              fontSize: 30, color: Colors.white),
                        ),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelStyle: GoogleFonts.inter(
                            color: Colors.white,
                          ),
                          labelText: 'Cep',
                          prefixIcon: const Icon(
                            Icons.maps_home_work,
                            color: Colors.amber,
                          ),
                        ),
                        style: GoogleFonts.inter(color: Colors.white),
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          TextInputMask(mask: '99999-999', placeholder: '_')
                        ],
                        controller: controller.cepController,
                        onChanged: ((cep) {
                          EasyDebounce.debounce(
                              'my-debouncer', // <-- An ID for this particular debouncer
                              const Duration(
                                  milliseconds:
                                      1000), // <-- The debounce duration
                              () => controller.getCEPAdress(cep));
                        }),
                        onSaved: ((value) => controller
                            .dataSrv.currentUsuario.endereco!.cep = value!),
                        validator: ((value) {
                          return controller.validateCep(value!);
                        }),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelStyle: GoogleFonts.inter(
                            color: Colors.white,
                          ),
                          labelText: 'Número',
                          prefixIcon: const Icon(
                            Icons.pin,
                            color: Colors.amber,
                          ),
                        ),
                        style: GoogleFonts.inter(
                          color: Colors.white,
                        ),
                        keyboardType: TextInputType.number,
                        controller: controller.numeroController,
                        onSaved: ((value) {
                          print('salvei Numero');
                          controller.dataSrv.currentUsuario.endereco!.numero =
                              value!;
                        }),
                        validator: ((value) {
                          return controller.validateNumero(value!);
                        }),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelStyle: GoogleFonts.inter(
                            color: Colors.white,
                          ),
                          labelText: 'Complemento',
                          prefixIcon: const Icon(
                            Icons.add_road,
                            color: Colors.amber,
                          ),
                        ),
                        style: GoogleFonts.inter(
                          color: Colors.white,
                        ),
                        keyboardType: TextInputType.text,
                        controller: controller.complementoController,
                        onSaved: ((value) => controller.dataSrv.currentUsuario
                            .endereco!.complemento = value!),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelStyle: GoogleFonts.inter(
                            color: Colors.white,
                          ),
                          labelText: 'Logradouro(Rua)',
                          prefixIcon: const Icon(
                            Icons.home,
                            color: Colors.amber,
                          ),
                        ),
                        style: GoogleFonts.inter(
                          color: Colors.white,
                        ),
                        keyboardType: TextInputType.text,
                        controller: controller.logradouroController,
                        onSaved: ((value) => controller.dataSrv.currentUsuario
                            .endereco!.logradouro = value!),
                        validator: ((value) {
                          return controller.validateLogradouro(value!);
                        }),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelStyle: GoogleFonts.inter(
                            color: Colors.white,
                          ),
                          labelText: 'Bairro',
                          prefixIcon: const Icon(
                            Icons.reorder_sharp,
                            color: Colors.amber,
                          ),
                        ),
                        style: GoogleFonts.inter(
                          color: Colors.white,
                        ),
                        keyboardType: TextInputType.text,
                        controller: controller.bairroController,
                        onSaved: ((value) => controller
                            .dataSrv.currentUsuario.endereco!.bairro = value!),
                        validator: ((value) {
                          return controller.validateBairro(value!);
                        }),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelStyle: GoogleFonts.inter(
                            color: Colors.white,
                          ),
                          labelText: 'Município',
                          prefixIcon: const Icon(
                            Icons.emoji_transportation,
                            color: Colors.amber,
                          ),
                        ),
                        style: GoogleFonts.inter(
                          color: Colors.white,
                        ),
                        keyboardType: TextInputType.text,
                        controller: controller.localidadeController,
                        onSaved: ((value) => controller.dataSrv.currentUsuario
                            .endereco!.localidade = value!),
                        validator: ((value) {
                          return controller.validateLocalidade(value!);
                        }),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelStyle: GoogleFonts.inter(
                            color: Colors.white,
                          ),
                          labelText: 'UF',
                          prefixIcon: const Icon(
                            Icons.flag,
                            color: Colors.amber,
                          ),
                        ),
                        style: GoogleFonts.inter(
                          color: Colors.white,
                        ),
                        keyboardType: TextInputType.text,
                        inputFormatters: [
                          TextInputMask(mask: 'AA', placeholder: '_')
                        ],
                        controller: controller.ufController,
                        onSaved: ((value) => controller
                            .dataSrv.currentUsuario.endereco!.uf = value!),
                        validator: ((value) {
                          return controller.validateUF(value!);
                        }),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      SizedBox(
                        width: 200,
                        child: ElevatedButton(
                          onPressed: () => controller.createUser(),
                          style: ButtonStyle(
                            shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                            backgroundColor:
                                MaterialStateProperty.all(Colors.amber),
                            padding:
                                MaterialStateProperty.all(const EdgeInsets.all(20)),
                          ),
                          child: Text(
                            "Criar Conta",
                            style: GoogleFonts.inter(
                                fontSize: 14, color: Colors.black),
                          ),
                        ),
                      ),
                      /*  SizedBox(
                        height: 300,
                      ), */
                    ],
                  ),
                ),
        ),
      ),
    );
  }
}
