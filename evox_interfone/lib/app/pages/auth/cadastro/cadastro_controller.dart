// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:evox_interfone/app/models/usuario.model.dart';
import 'package:evox_interfone/app/pages/auth/cadastro/cepApiResponse.model.dart';
import 'package:evox_interfone/app/models/endereco.model.dart';
import 'package:evox_interfone/app/services/auth.service.dart';
import 'package:evox_interfone/app/services/data.service.dart';
import 'package:dio/dio.dart';

class CadastroController extends GetxController {
  late final GlobalKey<FormState> cadastroFormKey;
  final AuthService authSrv = Get.find<AuthService>();
  final DataService dataSrv = Get.find<DataService>();
  late TextEditingController cepController,
      numeroController,
      logradouroController,
      complementoController,
      bairroController,
      localidadeController,
      ufController;
  late TextEditingController emailController, passwordController;
  late TextEditingController nomeController, phoneController;

  final Dio dio = Dio();
  @override
  void onInit() {
    cadastroFormKey = GlobalKey<FormState>();
    //Info basica
    emailController = TextEditingController();
    passwordController = TextEditingController();
    nomeController = TextEditingController();
    phoneController = TextEditingController();

    //Info residencia
    cepController = TextEditingController();
    numeroController = TextEditingController();
    logradouroController = TextEditingController();
    complementoController = TextEditingController();
    bairroController = TextEditingController();
    localidadeController = TextEditingController();
    ufController = TextEditingController();

    super.onInit();
  }

  @override
  void onReady() {
    dataSrv.currentUsuario = Usuario(
        nome: '',
        celular: '',
        email: '',
        endereco: null,
        tokenChamada: '',
        dataCadastro: null,
        dataAtualizacao: null,
        id: '',
        photo: '');
/*     dataSrv.currenSelectedMember.value = Profile(
        nome: '', celular: '', userId: Uuid().v4(), photo: '', isAdmin: true); */

    if (dataSrv.currentUsuario.endereco == null) {
      dataSrv.currentUsuario.endereco = Endereco(
          bairro: '',
          logradouro: '',
          uf: '',
          localidade: '',
          complemento: '',
          numero: '',
          cep: '');
    }

    super.onReady();
  }

  String? validateName(String value) {
    var retorno;
    if (value.isEmpty) {
      retorno = 'Nome inválido';
    }
    return retorno;
  }

  String? validatePhone(String value) {
    var retorno;
    if (value.isEmpty) {
      retorno = 'Celular inválido';
    }
    if (value.length < 15) {
      retorno = 'Digite um celular válido';
    }
    if (value.length > 16) {
      retorno = 'Digite um celular válido';
    }
    return retorno;
  }

  String? validateEmail(String value) {
    String? retorno;
    if (!GetUtils.isEmail(value)) {
      retorno = 'Email inválido';
    }
    return retorno;
  }

  String? validatePassword(String value) {
    String? retorno;
    if (value.length < 6) {
      retorno = 'Senha precisa ter mais de 6 caracteres';
    }
    return retorno;
  }

  String? validateCep(String value) {
    String? retorno;
    if (value.length < 9 || value == '') {
      retorno = 'Cep inválido';
    }
    return retorno;
  }

  String? validateNumero(String value) {
    String? retorno;
    if (value == '') {
      retorno = 'Numero inválido';
    }
    return retorno;
  }

  String? validateComplemento(String value) {
    String? retorno;
    if (value == '') {
      retorno = 'Complemento inválido';
    }
    return retorno;
  }

  String? validateLogradouro(String value) {
    String? retorno;
    if (value == '') {
      retorno = 'Logradouro inválido';
    }
    return retorno;
  }

  String? validateBairro(String value) {
    String? retorno;
    if (value.isEmpty) {
      retorno = 'Bairro inválido';
    }
    return retorno;
  }

  String? validateLocalidade(String value) {
    String? retorno;
    if (value.isEmpty) {
      retorno = 'Municipio inválido';
    }
    return retorno;
  }

  String? validateUF(String value) {
    String? retorno;
    if (value.isEmpty) {
      retorno = 'UF inválido';
    }
    return retorno;
  }

  bool checkLogin() {
    final retorno = cadastroFormKey.currentState!.validate();
    return retorno;
  }

  void createUser() {
    if (checkLogin()) {
      cadastroFormKey.currentState?.save();
      if (dataSrv.currentUsuario.id == '') {
        print('Isso funcionou? => ');
        print(dataSrv.currentUsuario.toJson());
        print(authSrv.tempPassword);
        dataSrv.currentUsuario.email = emailController.value.text;

        authSrv.signUserUp();
        cadastroFormKey.currentState?.reset();
      }
    } else {
      return;
    }
  }

  void getCEPAdress(String cep) async {
    cep = cep.replaceAll(new RegExp(r'[^\w\s]+'), '');
    print(cep);
    dio.get('https://viacep.com.br/ws/$cep/json/').then((_response) {
      var cepCompleto =
          CepApiResponse.fromJson(_response.data as Map<String, dynamic>);

      logradouroController.value =
          TextEditingValue(text: cepCompleto.logradouro);
      bairroController.value = TextEditingValue(text: cepCompleto.bairro);
      localidadeController.value =
          TextEditingValue(text: cepCompleto.localidade);

      ufController.value = TextEditingValue(text: cepCompleto.uf);
      dataSrv.currentUsuario.endereco!.bairro = cepCompleto.bairro;
      print(cepCompleto.bairro);
      print(_response);
    }).catchError((onError) {
      Get.snackbar('Cep inválido', '', backgroundColor: Colors.red);
      print(onError.toString());
    });
  }

  @override
  void onClose() {
    super.onClose();
  }
}
