import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:evox_interfone/app/services/auth.service.dart';
import 'package:evox_interfone/app/services/data.service.dart';

class SideBarMenuController extends GetxController {
  final DataService dataSrv = Get.find<DataService>();
  final AuthService authSrv = Get.find<AuthService>();
  final List<Map<String, dynamic>> appPages = [
    {'title': 'Tela Inicial', 'icon': FontAwesomeIcons.house, 'path': '/home'},
    {
      'title': 'Histórico',
      'icon': FontAwesomeIcons.clockRotateLeft,
      'path': '/historico'
    },
    // {'title': 'QrCode', 'icon': FontAwesomeIcons.qrcode, 'path': '/qr-code'},
    {
      'title': 'Configurações',
      'icon': FontAwesomeIcons.gear,
      'path': '/config'
    },
/*     {
      'title': 'Sair',
      'icon': FontAwesomeIcons.rightFromBracket,
      'path': '/sair'
    }, */
  ];
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  goTo(String pageName) {
    if (pageName == '/sair') {
      authSrv.signOut();
    } else {
      Navigator.of(Get.context!).pop();
      Get.offAllNamed(pageName);
    }
  }

  @override
  void onClose() {}
}
