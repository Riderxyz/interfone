class Endereco {
  Endereco({
    required this.bairro,
    required this.logradouro,
    required this.uf,
    required this.localidade,
    required this.complemento,
    required this.numero,
    required this.cep,
  });
  late String bairro;
  late String logradouro;
  late String uf;
  late String localidade;
  late String complemento;
  late String numero;
  late String cep;

  Endereco.fromJson(Map<String, dynamic> json) {
    bairro = json['bairro'];
    logradouro = json['logradouro'];
    uf = json['uf'];
    localidade = json['localidade'];
    complemento = json['complemento'];
    numero = json['numero'];
    cep = json['cep'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['bairro'] = bairro;
    _data['logradouro'] = logradouro;
    _data['uf'] = uf;
    _data['localidade'] = localidade;
    _data['complemento'] = complemento;
    _data['numero'] = numero;
    _data['cep'] = cep;
    return _data;
  }
}
