class Localizacao {
  late int lat = 0;
  late int long = 0;
  Localizacao({
    required this.lat,
    required this.long,
  });
  // ignore: unnecessary_question_mark
  Localizacao.fromJson(Map<dynamic, dynamic> json)
      : this(
          lat: json['lat'] as int,
          long: json['long'] as int,
        );

  Map<String, Object> toJson() {
    return {
      'lat': lat,
      'long': long,
    };
  }
}
