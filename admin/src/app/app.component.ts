import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { collapseOnLeaveAnimation, expandOnEnterAnimation, fadeInExpandOnEnterAnimation } from "angular-animations";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  animations:[expandOnEnterAnimation({
    delay: 1000
  }),
  fadeInExpandOnEnterAnimation(),
  collapseOnLeaveAnimation()]
})
export class AppComponent implements OnInit {
  title = "admin";
showheader = true;
  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.router.events.subscribe((obs) => {
if (this.router.url.includes('/login')) {
  this.showheader = false;
} else {
 this.showheader = true
}
    })
  }
}
