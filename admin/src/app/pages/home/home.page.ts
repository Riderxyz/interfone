import { DataService } from "./../../services/data.service";
import { Component, OnInit } from "@angular/core";
import * as uuid from "uuid-random";
@Component({
  templateUrl: "./home.page.html",
  styleUrls: ["./home.page.scss"],
})
export class HomePage implements OnInit {
  callIdToken = "";
  totalUserNum = 0;
  constructor(private dataSrv: DataService) {}
  ngOnInit(): void {
    console.clear();
    this.dataSrv.userList.subscribe((res) => {
      this.totalUserNum = res.length;
    });
    this.dataSrv.test().subscribe(
      (rs) => {
        console.log(rs);
      },
      (err) => {
        console.log(err);
      }
    );
    this.callIdToken = uuid();
  }
  mudarToken() {
    this.callIdToken = uuid();
  }
}
