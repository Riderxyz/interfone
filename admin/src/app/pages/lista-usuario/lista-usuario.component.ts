import { QRCodeService } from './../../services/qrCode.service';
import { Router } from "@angular/router";
import { LegacyUsuarioInteface } from "../../models/legacy_usuario.interface";
import { CriarUsuarioComponent } from "./../../components/criar-usuario/criar-usuario.component";
import { config } from "./../../services/config";
import { ToastService } from "./../../services/toast.service";
import { DataService } from "./../../services/data.service";
import { Component, OnInit } from "@angular/core";
import {
  GridOptions,
  GridApi,
  ColumnApi,
  ColDef,
  RowDoubleClickedEvent,
  SelectionChangedEvent,
  RowClickedEvent,
} from "ag-grid-community";
import { zoomInUpOnEnterAnimation } from "angular-animations";
import * as lodash from "lodash";
import Swal, { SweetAlertOptions } from "sweetalert2";
@Component({
  selector: "app-lista-usuario",
  templateUrl: "./lista-usuario.component.html",
  styleUrls: ["./lista-usuario.component.scss"],
  animations: [zoomInUpOnEnterAnimation()],
})
export class ListaUsuarioPage implements OnInit {
  gridOptions: GridOptions;
  usuarioArr: LegacyUsuarioInteface[] = [];
  selectedRows: any[] = [];
  filtro = "";
  allowEdit = false;
  displayCriarUsuario = false;
  displayEditarUsuario = false;

  constructor(
    private dataSrv: DataService,
    private toastSrv: ToastService,
    private router: Router,
    private qrCodeSrv: QRCodeService
  ) {}

  ngOnInit(): void {
    this.dataSrv.userList.subscribe((res) => {
      console.log(res);
      this.usuarioArr = res;
    });
    /*    this.dataSrv.getUserHistoricoChamada('x87VWdE8Z5ZtuPDlBnFZ7NAPt5v1').subscribe((res)=> {

    console.log(JSON.stringify(res));

   }) */
  }
  get createColumnDefs(): Array<ColDef> {
    return config.columnDef.legacyUsuarioGrid;
  }
  get colunasFiltraveis() {
    return ["nome", "email"];
  }

  onGridSelectedRow(event) {
    this.selectedRows = event;
    if (this.selectedRows.length === 1) {
      this.allowEdit = true;
    } else {
      this.allowEdit = false;
    }
  }

  onGridDobleClick(event) {
    this.editItem(event);
  }

  addNewItem(): void {
    this.selectedRows[0] = null;
    this.displayCriarUsuario = true;
  }
  editItem(rowData?) {
    if (rowData === undefined) {
      rowData = this.selectedRows[0];
    }
    this.displayCriarUsuario = true;
    console.log(rowData);

    /*    const dialogRef = this.dialog.open(MaterialModal, {
      minWidth: "250px",
      data: rowData,
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
    }); */
  }

  createQrCodePage() {
    const rowData: LegacyUsuarioInteface = this.selectedRows[0];
    console.log(rowData);
    this.qrCodeSrv.generatePDF(config.pdfUserQrCodeConfigs(rowData.tokenChamada))
  }
  closeDialog() {
    this.displayCriarUsuario = false;
  }
  tes() {
    this.toastSrv.testLoad();
  }
  deleteItem() {
    const toastConfig: SweetAlertOptions = {
      title: "Tem certeza",
      text: "Não será possivel reverter esta ação",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Sim, delete-os!",
    };
    this.toastSrv.showConfirmDialog(toastConfig).then(async (result) => {
      if (result.value) {
        Swal.fire({
          title: "Deletando usuarios!",
          didOpen: () => {
            Swal.showLoading();
          },
          willClose: () => {},
        });
        this.selectedRows.forEach((ev: LegacyUsuarioInteface, index) => {
          console.log(ev);
          this.dataSrv.deletarUsuario(ev.id).subscribe(
            () => {
              if (index === this.selectedRows.length - 1) {
                Swal.close();
                Swal.fire(
                  "Deletados",
                  "Arquivos selecionados foram deletados",
                  "success"
                );
              }
            },
            (err) => {
              console.log(err);
              Swal.close();
            }
          );
        });
      }
    });
  }
}
