import { EnderecoInterface } from "./endereco.interface";
import * as uuid from "uuid-random";
export interface UsuarioInteface {
  nome: string;
  celular: string;
  userId: string;
  photo: string;
  tokenChamada: string;
  email: string;
  dataCadastro: number;
  dataAtualizacao: number;
  endereco: EnderecoInterface;
}

export const newUsuario = () => {
  let retorno: UsuarioInteface = {
    nome: "",
    celular: "",
    //isAdmin: false,
    userId: uuid(),
    photo: "",
    tokenChamada: "",
    email: "",
    dataCadastro: 0,
    dataAtualizacao: 0,
    endereco: {
      bairro: "",
      logradouro: "",
      uf: "",
      localidade: "",
      complemento: "",
      numero: "",
      cep: "",
    },
  };
  return retorno;
};
