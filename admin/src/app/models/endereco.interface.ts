export interface EnderecoInterface {
  bairro: string;
  logradouro: string;
  uf: string;
  localidade: string;
  complemento: string;
  numero: string;
  cep: string;
}
