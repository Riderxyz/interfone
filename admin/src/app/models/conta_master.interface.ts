import { UsuarioInteface } from './usuario.interface';
import { EnderecoInterface } from './endereco.interface';
import * as uuid from 'uuid-random';
export interface masterConta {
  endereco?: EnderecoInterface;
  tokenChamada: string;
  email: string;
  id: string;
  data_atualizacao: number;
  data_cadastro: number;
}

export const newMasterConta = (() => {
  let retorno: masterConta = {
    tokenChamada: uuid(),
    email: "",
    data_cadastro: 0,
    data_atualizacao: 0,
    id: "",
  }
  return retorno;
})
