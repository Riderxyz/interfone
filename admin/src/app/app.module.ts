import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { FormsModule } from '@angular/forms';

import { HomePage } from './pages/home/home.page';
import { ListaUsuarioPage } from './pages/lista-usuario/lista-usuario.component';
import { LoginPage } from './pages/login/login.page';

import { QRCodeModule } from 'angularx-qrcode';
import { ComponentModule } from './components/components.module';
import { CdkModule } from './cdk.module';
import { PrimeNGModule } from './primeNG.module';
import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule } from '@angular/common/http';

//Services
import { DataService } from './services/data.service';
import { ToastService } from './services/toast.service';
import { PWAService } from './services/pwa.service';
import { AuthService } from './services/auth.service';
import { QRCodeService } from './services/qrCode.service';



import { environment } from './../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';

const AngularFire = [
  AngularFireModule.initializeApp(environment.firebase),
  AngularFireDatabaseModule,
  AngularFirestoreModule.enablePersistence(),
  AngularFireAuthModule,
  AngularFireMessagingModule,
  AngularFireStorageModule,
  AngularFireAnalyticsModule,
];
@NgModule({
  declarations: [AppComponent, HomePage, ListaUsuarioPage, LoginPage],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    CdkModule,
    AppRoutingModule,
    ComponentModule,
    QRCodeModule,
    HttpClientModule,
    PrimeNGModule,
    // AngularFire
    ...AngularFire,
    // Grid
    AgGridModule.withComponents([]),

    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000',
    }),
  ],
  providers: [
    AuthService,
    PWAService,
    ToastService,
    DataService,
    QRCodeService
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
//
