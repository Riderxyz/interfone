import { LoginPage } from './pages/login/login.page';
import { ListaUsuarioPage } from './pages/lista-usuario/lista-usuario.component';
import { HomePage } from './pages/home/home.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginPage,
  },
  {
    path: 'home',
    component: HomePage,
  },
  {
    path: 'listaUsuario',
    component: ListaUsuarioPage,
  },
/*   {
    path: 'qrCodeGen',
    component: QrCodeGenPage,
  },
  {
    path: 'placaUsuario/:userID',
    component: PlacaUsuarioPage,
  }, */
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
