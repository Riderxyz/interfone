import { ColDef } from "ag-grid-community";
import * as moment from "moment";
export const config = {
  /*  rxjsCentralKeys: {
    GridResize: 'gridResize',
    GridReady: 'gridReady',
    ChangeToMobile: 'changeToMobile',
    ChangeToWeb: 'changeToWeb',
    pwaUpdateRequest: 'pwaUpdateRequest',
    lastUpdate: 'lastUpdate',
    openSideMenu: 'openSideMenu',
    closeSideMenu: 'closeSideMenu',
    onGridFilter: 'onGridFilter'
  }, */
  localStorageKey: {
    firebaseAuthCred: "firebaseAuthCred",
  },
  getRandomId: () => {
    return Math.random().toString(36).substr(2, 9);
  },
  url: {
    dbUser: "usuarios",
    dbAdmins: "administradores",
    adduserAPI:
      "https://us-central1-interfone-a2139.cloudfunctions.net/addUserToFirebase",
    pushNotificationAPI:
      "https://us-central1-arteconecta-27292.cloudfunctions.net/SendNotification",
  },
  columnDef: {
    legacyUsuarioGrid: [
      {
        headerName: "nome".toLocaleUpperCase(),
        field: "nome",
        width: 25,
        checkboxSelection: (params) => {
          return params.columnApi.getRowGroupColumns().length === 0;
        },
        headerCheckboxSelection: (params) => {
          return params.columnApi.getRowGroupColumns().length === 0;
        },
      },
      {
        headerName: "email".toLocaleUpperCase(),
        field: "email",
        width: 15,
        autoHeight: true,
      },
      {
        headerName: "Token Cadastrado".toLocaleUpperCase(),
        field: "tokenChamada",
        width: 60,
        autoHeight: true,
      },
      {
        headerName: "ID",
        field: "id",
        maxWidth: 0,
        hide: true,
      },
    ] as ColDef[],
    usuarioGrid: [
      {
        headerName: "email".toLocaleUpperCase(),
        field: "email",
        width: 25,
        autoHeight: true,
        checkboxSelection: (params) => {
          return params.columnApi.getRowGroupColumns().length === 0;
        },
        headerCheckboxSelection: (params) => {
          return params.columnApi.getRowGroupColumns().length === 0;
        },
      },
      {
        headerName: "ID",
        field: "id",
        maxWidth: 0,
        hide: true,
      },
      {
        headerName: "Cadastrado em".toLocaleUpperCase(),
        field: "data_cadastro",
        width: 30,
        autoHeight: true,
      },
      {
        headerName: "Data da Ultima Atualização".toLocaleUpperCase(),
        field: "data_atualizacao",
        width: 10,
        autoHeight: true,
      },
    ] as ColDef[],
  },
  pdfUserQrCodeConfigs: (userToken) => {
    let pdfScheme = {
      pageSize: "A4",
      background: function () {
        return {
          canvas: [
            {
              type: "rect",
              x: 0,
              y: 0,
              w: 595.28,
              h: 841.89,
              color: "#000000",
            },
          ],
        };
      },
      content: [
        {
          text: "I N T E R F O N E",
          fontSize: 50,
          bold: true,
          alignment: "center",
          color: "#FFFFFF",
          margin: [0, 0, 0, 40],
        },
        {
          text: "Bem Vindo(a)",
          fontSize: 30,
          alignment: "center",
          color: "#FFFFFF",
          margin: [0, 0, 0, 20],
        },
        {
          text: "Para ligar para este usuario, aponte a camera para o codigo abaixo",
          fontSize: 30,
          alignment: "center",
          color: "#FFFFFF",
          margin: [0, 0, 0, 90],
        },
        {
          alignment: "center",
          columns: [
            {
              qr: "https://visitanteprototipo.netlify.app/#/home/" + userToken,
              fit: "350",
              foreground: "white",
              background: "black",
              margin: [0, 0, 0, 20],
            },
          ],
        },
      ],
    };
    return pdfScheme;
  },
};
