import { Injectable } from '@angular/core';
/* import * as QRCode from 'qrcode'; */
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
@Injectable()
export class QRCodeService {
  constructor() {


  }

/*  async returnQRCodeImage(qrcodeToken: string) {
return QRCode.toDataURL(qrcodeToken)
}
 */
generatePDF(pdfDoc) {
  //const documentDefinition = { content: 'This is an sample PDF printed with pdfMake' };
  pdfMake.createPdf(pdfDoc).open();
}
}
