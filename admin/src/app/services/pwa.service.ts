import { Injectable } from '@angular/core';

import { SwUpdate } from '@angular/service-worker';
import { ToastService } from './toast.service';
import { SweetAlertOptions } from 'sweetalert2';
@Injectable()
export class PWAService {
  updateToastConfig: SweetAlertOptions = {
    position: 'bottom',
    title: 'Atualização disponivel',
    confirmButtonText: 'carregar atualização',
    customClass: {
      title: 'onErrorToast',
      container: 'onErrorToast'
    },
    // tslint:disable-next-line: max-line-length
    background: 'linear-gradient(to left, #a45555, #af5168, #b55181, #b1559e, #a060bd, #8373d4, #5b85e3, #0094eb, #00a4db, #00adb5, #20af87, #7aad63)',
    timer: 6000,
    toast: true,
    timerProgressBar: true
  };
  constructor(private swUpdate: SwUpdate, private toastSrv: ToastService) {

 /*    this.swUpdate.checkForUpdate().then(() => {
    }); */
    if (!this.swUpdate.isEnabled) {
      console.log('Nope 🙁');
    } else {
      console.log('Yeeeaayy😃');
    }

    this.swUpdate.available.subscribe(evt => {
      this.toastSrv.showConfirmDialog(this.updateToastConfig).then((res) => {
        window.location.reload();
        console.log('estou aqui no dismiss');
      });
    });
  }

  init() {
    console.log('Esta função ativa o serviço de PWA');
  }
}
