import { ToastService } from './toast.service';
import { config } from "./config";
import { LegacyUsuarioInteface } from "../models/legacy_usuario.interface";
import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from "@angular/fire/firestore";
import { HttpClient } from '@angular/common/http';
@Injectable()
export class AuthService {
  criandoUsuario = false;
  constructor(
    private authF: AngularFireAuth) {}



  login(email, password) {
    this.authF.signInWithEmailAndPassword(email, password).then((res) => {
      res.credential;
      localStorage.setItem(
        config.localStorageKey.firebaseAuthCred,
        JSON.stringify(res.credential)
      );
    });
  }
}
