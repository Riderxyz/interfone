import { Injectable } from "@angular/core";
import Swal, { SweetAlertOptions } from "sweetalert2";
import { Subject } from 'rxjs';
type Severities = 'success' | 'info' | 'warn' | 'error';
@Injectable()
export class ToastService {

  notificationChange: Subject<Object> = new Subject<{
    severity: Severities;
    summary: string;
    detail: string;
    life: number;
  }>();
  constructor() {}

  showToast(config: SweetAlertOptions) {
    /* title: 'Tem certeza',
            text: "Não será possivel reverter esta ação",
            icon: 'warning',
            showCancelButton: true,
            toast:true,
            position: 'top-right',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, delete-os!' */
    Swal.fire(config);
  }

  showConfirmDialog(config: SweetAlertOptions) {
    return Swal.fire(config);
  }

  testLoad() {
    Swal.fire({
      title: 'Auto close alert!',
      html: 'I will close in X milliseconds.',
      timer: 2000,
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading()
        /* const b = Swal.getHtmlContainer().querySelector('b')
        timerInterval = setInterval(() => {
          b.textContent = Swal.getTimerLeft()
        }, 100) */
      },
      willClose: () => {
      //  clearInterval(timerInterval)
      }
    }).then((result) => {
      /* Read more about handling dismissals below */
      if (result.dismiss === Swal.DismissReason.timer) {
        console.log('I was closed by the timer')
      }
    })
  }

  showToastSucess(title, subtitle?) {
    if (subtitle === undefined) {
      subtitle = null;
    }
    Swal.fire({
     title: title + '!',
            text: subtitle,
            icon: 'success',
            customClass: {
                title: 'onSucessToast',
                container: 'onSucessToast'
            },
            timerProgressBar: true,
            timer: 5000,
            showConfirmButton: false,
            showCancelButton: false,
            background: 'linear-gradient(315deg, #7ee8fa 0%, #80ff72 74%)',
            toast: true,

            //  timer: 3000,
            position: 'top-right',
    });
  }
  showToastError(title, subtitle?) {
    if (subtitle === undefined) {
      subtitle = null;
    }
    Swal.fire({
      title: title + ".",
      text: subtitle,
      icon: "error",
      timer: 10000,

      timerProgressBar: true,
      customClass: {
        title: "onErrorToast",
        container: "onErrorToast",
      },
      background: "linear-gradient(to left, #ff0095, #ff0000)",
      /*             showCancelButton: true, */
      toast: true,
    //  timer: 3000,
      position: "top-right",
    });
  }


  /**
   *
   *
   * @param {Severities} severity tipo de toast que pode ser: success, info, warn, error
   * @param {string} summary Titulo do Toast
   * @param {string} detail Subtitulo do Toast
   * @param {number} life Tempo de vida do Toast
   * @memberof ToastService
   */
  notify(severity: Severities, summary: string, detail: string, life: number) {
    this.notificationChange.next({ severity, summary, detail, life });
  }
}
