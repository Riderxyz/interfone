import { EnderecoInterface } from "./../models/endereco.interface";
import { LegacyUsuarioInteface } from "../models/legacy_usuario.interface";
import { ToastService } from "./toast.service";
import { config } from "./config";
import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { tap, take } from "rxjs/operators";
import { UsuarioInteface } from "../models/usuario.interface";
@Injectable()
export class DataService {
  private viaCepUrl = "https://viacep.com.br/ws";
  constructor(
    private db: AngularFirestore,
    private toastSrv: ToastService,
    private http: HttpClient
  ) {}

  get userList() {
    return this.db
      .collection<LegacyUsuarioInteface>(config.url.dbUser)
      .valueChanges();
  }
  //
  getUserHistoricoChamada(userID) {
    return this.db
      .collection(config.url.dbUser)
      .doc(userID)
      .collection("historicoChamadas")
      .valueChanges();
  }

  getSpecificUser(userID: string) {
    return this.db
      .collection(config.url.dbUser)
      .doc<LegacyUsuarioInteface>(userID)
      .valueChanges();
  }
  getMasterContaPerfis(userID: string) {
    return this.db
    .collection(config.url.dbUser)
    .doc(userID)
    .collection<UsuarioInteface>("contasRelacionadas")
    .valueChanges();
  }

  getEndereco(cep: string) {
    const url = `${this.viaCepUrl}/${cep}/json`;
    return this.http
      .get<EnderecoInterface>(url)
      .pipe(tap((_) => console.log(`fetched endereco cep=${cep}`)));
  }

  criarUsuarioNormal(userInfo: LegacyUsuarioInteface) {
    return this.http.post(config.url.adduserAPI, {
      email: userInfo.email,
      password: "123456",
      name: userInfo.nome,
    });
  }
  salvarEdicaoUsuario(userInfo: LegacyUsuarioInteface) {
    return this.db
      .collection(config.url.dbUser)
      .doc(userInfo.id)
      .update(userInfo);
  }

  deletarUsuario(userIdForDelete: string) {
    return this.http.post(
      "https://us-central1-interfone-a2139.cloudfunctions.net/deleteUserFromFirebase",
      {
        id: userIdForDelete,
      }
    );
  }

  test() {
    let header = new HttpHeaders();
    header.append(
      "Authorization",
      "Basic OTA4MjBlZGI4YjM2NDc2NmFlMzRiYWJiMGU0MTgyYjU6MjhmMDlhMTY1OTM4NGI1MTg3NzEzNzc1OWFiZWZiZDk="
    );
    header.append("Content-Type", "application/json");
    return this.http.get("https://api.agora.io/dev/v1/projects", {
      headers: header,
    });
  }

  set deleteFromDB(ev: { arr: any[]; path: string }) {
    /*     ev.arr.forEach((element) => {
      this.db.list(config.url.base + ev.path).remove(element.id);
    }); */
  }
}
