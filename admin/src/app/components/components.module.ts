import { ToastComponent } from "./toast/toast.component";
import { GridComponent } from "./grid/grid.component";
import { PrimeNGModule } from "./../primeNG.module";
import { NgModule } from "@angular/core";
import { CriarUsuarioComponent } from "./criar-usuario/criar-usuario.component";
import { HeaderComponent } from "./header/header.component";
import { AgGridModule } from "ag-grid-angular";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PrimeNGModule,
    AgGridModule.withComponents(),
  ],
  exports: [
    CriarUsuarioComponent,
    HeaderComponent,
    GridComponent,
    ToastComponent,
  ],
  declarations: [
    CriarUsuarioComponent,
    HeaderComponent,
    GridComponent,
    ToastComponent,
  ],
  providers: [],
})
export class ComponentModule {}
