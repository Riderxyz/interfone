import { newUsuario, UsuarioInteface } from "./../../models/usuario.interface";
import {
  masterConta,
  newMasterConta,
} from "./../../models/conta_master.interface";
import { DataService } from "./../../services/data.service";
import { AuthService } from "./../../services/auth.service";
import { LegacyUsuarioInteface } from "../../models/legacy_usuario.interface";
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  SimpleChanges,
} from "@angular/core";
import { NgForm } from "@angular/forms";
import { zoomInOnEnterAnimation } from "angular-animations";
import { ToastService } from "src/app/services/toast.service";

@Component({
  selector: "app-criar-usuario",
  templateUrl: "./criar-usuario.component.html",
  styleUrls: ["./criar-usuario.component.scss"],
  animations: [zoomInOnEnterAnimation()],
})
export class CriarUsuarioComponent implements OnInit {
  masterContaPerfilArr: UsuarioInteface[] = [];
  @Input() masterContaObj: masterConta;
  activeTabIndex = 0;

  @Output() fecharDialog = new EventEmitter<boolean>();
  isEditing = false;
  accordionisOpen = false;
  constructor(
    public AuthSrv: AuthService,
    private toastSrv: ToastService,
    private dataSrv: DataService
  ) {}

  ngOnInit(): void {}
  ngOnChanges(changes: SimpleChanges): void {
    this.accordionisOpen = true;
    console.log("Linha 36", this.accordionisOpen);
    if (!this.masterContaObj) {
      this.isEditing = false;
      this.masterContaObj = newMasterConta();
      console.log(JSON.stringify(this.masterContaObj));
    } else {
      this.isEditing = true;
    }
  }

  cancel() {
    this.fecharDialog.emit(false);
  }
  adicionarPerfil() {
    let newPerfil = newUsuario();
    console.log("te", JSON.stringify(newPerfil));
    this.masterContaPerfilArr.push(newPerfil);
  }
  getMasterContaPerfis() {
    this.dataSrv
      .getMasterContaPerfis(this.masterContaObj.id)
      .subscribe((res) => {
        this.masterContaPerfilArr = res;
      });
  }

  salvar(form: NgForm) {
    /*  console.log(form);
    console.log(form.valid);
    this.AuthSrv.criandoUsuario = true;
    if (form.valid) {
      console.log(this.masterContaObj);
      if (this.isEditing) {
        this.dataSrv
          .salvarEdicaoUsuario(this.masterContaObj)
          .then(() => {
            this.AuthSrv.criandoUsuario = false;
            this.toastSrv.showToastSucess(
              "Dados do usuario modificados com sucesso"
            );
            this.fecharDialog.emit(false);
          })
          .catch((err) => {
            this.AuthSrv.criandoUsuario = false;
            this.toastSrv.showToastError(
              "Ocorreu um erro durante a operação",
              "Tente novamente"
            );
            this.fecharDialog.emit(false);
          });
      } else {
        this.dataSrv.criarUsuarioNormal(this.masterContaObj).subscribe(
          (res: any) => {
            if (res.response === 200) {
              this.fecharDialog.emit(false);
              this.AuthSrv.criandoUsuario = false;
              this.toastSrv.showToastSucess(res.msg);
            }
          },
          (err) => {
            console.log(err);
            this.fecharDialog.emit(false);
            this.AuthSrv.criandoUsuario = false;
            this.toastSrv.showToastError(err.error.msg);
          }
        );
      }
    } */
  }
}
