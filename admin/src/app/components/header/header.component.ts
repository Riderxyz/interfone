import { Component, OnInit } from '@angular/core';
import { MenuItem } from "primeng/api";
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  dockItems: MenuItem[] = [];
  constructor() { }

  ngOnInit(): void {

    this.dockItems = [
      {
        label: "Home",
        tooltipOptions: {
          tooltipLabel: "Home",
          tooltipPosition: "bottom",
        },
        routerLink: ["/home"],
        icon: "fas fa-home",
      },
      {
        label: "Lista de usuario",
        tooltipOptions: {
          tooltipLabel: "Ver lista de usuario",
          tooltipPosition: "top",
        },
        routerLink: ["/listaUsuario"],
        icon: "fas fa-users",
      },
/*       {
        label: "Gerar QRCode",
        tooltipOptions: {
          tooltipLabel: "Gerar QRCode",
          tooltipPosition: "top",
        },
        routerLink: ["/qrCodeGen"],
        icon: "fas fa-qrcode",
      }, */
    ];
  }

}
