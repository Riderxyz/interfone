import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { initializeApp,provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../environments/environment';
import { provideAnalytics,getAnalytics,ScreenTrackingService,UserTrackingService } from '@angular/fire/analytics';
import { provideAuth,getAuth } from '@angular/fire/auth';
import { LoadingPage } from './pages/loading/loading.page';

import { AngularFireModule  } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';


const AngularFire = [
  AngularFireModule.initializeApp(environment.firebase),
  AngularFirestoreModule,
]


@NgModule({
  declarations: [
    AppComponent,
    LoadingPage
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ...AngularFire
  ],
  providers: [
    ScreenTrackingService,UserTrackingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
