import { LoadingPage } from './pages/loading/loading.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'loading/d76-ea83-4980-8818-70b70012251c',
    pathMatch: 'full',
  },
  { path: 'loading/:userID', component: LoadingPage },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
