import { environment } from './../../../environments/environment';
import { UsuarioInterface } from './../../models/usuario.interface';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { AngularFirestore } from '@angular/fire/compat/firestore';

//pdfMake.vfs = pdfFonts.pdfMake.vfs;
@Component({
  selector: 'app-loading',
  templateUrl: './loading.page.html',
  styleUrls: ['./loading.page.scss']
})
export class LoadingPage implements OnInit {
pdfMakerVFS = pdfMake.vfs;
usuarioObj!: UsuarioInterface;
constructor(
  private route: ActivatedRoute,
  private db: AngularFirestore,
) { }

ngOnInit(): void {
  this.pdfMakerVFS = pdfFonts.pdfMake.vfs;
  this.findUser(this.route.snapshot.paramMap.get('userID')!)
  }


  findUser(qrCodeId: string) {
    console.log(qrCodeId)
    const user = this.db.collection('/usuarios', (res) =>
      res.where('tokenChamada', '==', qrCodeId)
    );
    user.valueChanges().subscribe((resWith: any[]) => {
      let res: any[] = [];
      res = resWith;
      console.log('pesquisa de codigos qR', res);
      console.log(res[0]);
      setTimeout(() => {
        this.createQrCodePage(res[0].tokenChamada);
      }, 1500);
    })/* .unsubscribe(); */
  }


  createQrCodePage(qrToken: string) {
    this.generatePDF(environment.pdfUserQrCodeConfigs(qrToken))
  }

  generatePDF(pdfDoc: any) {
    //const documentDefinition = { content: 'This is an sample PDF printed with pdfMake' };
    pdfMake.createPdf(pdfDoc).open();
  }

}
