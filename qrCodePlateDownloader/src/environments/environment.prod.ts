export const environment = {
  firebase: {
    projectId: 'interfone-a2139',
    appId: '1:760319116030:web:d646c0050d1bb2ec3e5baf',
    databaseURL: 'https://interfone-a2139-default-rtdb.firebaseio.com',
    storageBucket: 'interfone-a2139.appspot.com',
    locationId: 'southamerica-east1',
    apiKey: 'AIzaSyDM0vaIQtgnWxY204bEmCVVRkasiYixy68',
    authDomain: 'interfone-a2139.firebaseapp.com',
    messagingSenderId: '760319116030',
    measurementId: 'G-XPSPK2FNPL',
  },
  production: true,
  pdfUserQrCodeConfigs: (userToken: string) => {
    let pdfScheme = {
      pageSize: "A4",
      background: function () {
        return {
          canvas: [
            {
              type: "rect",
              x: 0,
              y: 0,
              w: 595.28,
              h: 841.89,
              color: "#000000",
            },
          ],
        };
      },
      content: [
        {
          text: "I N T E R F O N E",
          fontSize: 50,
          bold: true,
          alignment: "center",
          color: "#FFFFFF",
          margin: [0, 0, 0, 40],
        },
        {
          text: "Bem Vindo(a)",
          fontSize: 30,
          alignment: "center",
          color: "#FFFFFF",
          margin: [0, 0, 0, 20],
        },
        {
          text: "Para ligar para este usuario, aponte a camera para o codigo abaixo",
          fontSize: 30,
          alignment: "center",
          color: "#FFFFFF",
          margin: [0, 0, 0, 90],
        },
        {
          alignment: "center",
          columns: [
            {
              qr: "https://visitanteprototipo.netlify.app/#/home/" + userToken,
              fit: "350",
              foreground: "white",
              background: "black",
              margin: [0, 0, 0, 20],
            },
          ],
        },
      ],
    };
    return pdfScheme;
  },
};
