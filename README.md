# Evox Interfone

Bem vindo ao projeto evox_interfone

## Sobre:

### Admin:

Dashboard de controle de usuarios para todo o projeto Feito em Angular 12. Capaz de fazer CRUD dos usuarios, gerar PDF com o QR Code para o usuario imprimir e colar na sua casa(Precisa de ajustes e em como lidar com o QRCode tambem), envio de push notification para usuarios com ofertas e etc.

Para rodar o webApp, garanta que voce tenha instalado uma versao do nodeJs de no minimo 18. A seguir, siga esses comandos

    npm install @angular/cli -g --save
Este comando para instalar o cli do Angular. Em seguida, rode esse comando:

    npm install
Para instalar todos os pacotes e então por ultimo o comando:

    ng serve -o
para rodar o programa

### CloudFunction

Pasta responsavel pela funções de backend. Possui duas subpastas:
* functions: responsavel pelas CloudFunctions no Firebase. Todas funções aqui e suas subpastas estão rodando no Firebase. 
* localFunctionsTest: Um local de testes para as cloud functions. Para rodar as funções na pasta de localFunctionsTest, use o [nodemon](https://nodemon.io).

### Evox_interfone

Aqui esta o app. Para rodar voce precisa seguir esses passos:
* baixe e instale o [Flutter SDK](https://docs.flutter.dev/get-started/install)
* Adicione o [SDK ao path do windows ou linux](https://docs.flutter.dev/get-started/install/windows/mobile#update-your-windows-path-variable).
* Após configurar o Flutter SDK, abra a pasta no VSCode e com o termina aberto tambem(seja usando um terminal externo ou do proprio VSCode) rode o comando: `flutter pub install`. Esse comando ira instalar todos os pacotes direto do [pub.dev](https://pub.dev).
*  Após fazer isso, instale essa [extensão](https://marketplace.visualstudio.com/items?itemName=Dart-Code.flutter) no VSCode e então rode o projeto com a tecla F5. Isso fara o VSCode pedir por um emulador ou um dispositivo android(você escolhe) e então ele ira buildar e rodar o aplicativo para você.