import { config } from './../../services/config';
import { UsuarioInterface } from './../../models/usuario.interface';
import { DataService } from './../../services/data.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  Camera,
  CameraDirection,
  CameraResultType,
  CameraSource,
  Photo,
} from '@capacitor/camera';
import { Geolocation } from '@capacitor/geolocation';
import {
  AngularFireStorage,
  AngularFireStorageReference,
  AngularFireUploadTask,
} from '@angular/fire/compat/storage';
import { NgForm } from '@angular/forms';
import {
  bounceInRightOnEnterAnimation,
  bounceOutLeftOnLeaveAnimation,
  flipInXOnEnterAnimation,
} from 'angular-animations';
import { delay } from 'rxjs/operators';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    flipInXOnEnterAnimation({
      anchor: 'fotoAceita',
    }),
    bounceInRightOnEnterAnimation(),
    bounceOutLeftOnLeaveAnimation(),
  ],
})
export class HomePage implements OnInit {
  [x: string]: any;
  imgToShow = '';
  proprietarioOBj!: UsuarioInterface;
  isGpsDataAvailable = false;
  isLoadingPhotoPage = false;
  isUploadPhotoProcessOngoing = false;
  progressBarStyle = {
    height: '30px',
    width: '30vh',
  };
  progressBarValue = 0;
  constructor(
    public dataSrv: DataService,
    private routeNav: Router,
    private route: ActivatedRoute,
    private afStorage: AngularFireStorage,
    private toastSrv: ToastService
  ) {}

  ngOnInit(): void {
    this.dataSrv.userCallingObj.nome = '';
    console.warn(sessionStorage.getItem(config.agoraChannelKey));
    this.dataSrv.findUser(this.route.snapshot.paramMap.get('tokenChamada')!);
    sessionStorage.setItem(
      config.agoraChannelKey,
      this.route.snapshot.paramMap.get('tokenChamada')!
    );
    this.askGeoPermision();
  }

  askGeoPermision() {
    Geolocation.checkPermissions()
      .then((res) => {
        console.log('12345', res);
        this.dataSrv.updateGeoLocation();
      })
      .catch((err) => {
        console.log(err);
     //   this.gettingGPSLocation = false;
      });
  }

  goToPhoto() {
    console.log(123456);
    this.routeNav.navigateByUrl('/photo');
  }

  async onPhotoCall() {
    this.isLoadingPhotoPage = true;
    try {
      const image = await Camera.getPhoto({
        quality: 50,
        // allowEditing: true,
        resultType: CameraResultType.DataUrl,
        allowEditing: false,
        width: 100,
        height: 100,
        direction: CameraDirection.Front,
        source: CameraSource.Camera,

      });
      console.log(image);
      this.imgToShow = image.dataUrl as any;
      console.log(this.imgToShow);
      // 'data:image/png;base64,' +  image.base64String;
      //this.isLoadingPhotoPage = false;
      await this.uploadImageToCall(image);
    } catch (error) {
this.toastSrv.notify('error', 'Erro ao tentar tirar foto', '', 3000)
    }


  }

  async uploadImageToCall(image: Photo) {
    console.clear();
    this.isUploadPhotoProcessOngoing = true;
    const response = await fetch(image.dataUrl as any);
    const fileName = new Date().getTime();
    const blob = await response.blob();
    const fileReference =
      '/usuario/' +
      this.dataSrv.userCallingObj.idToCallTo +
      '/temp/' +
      fileName +
      '.png';
    const fileRef: AngularFireStorageReference =
      this.afStorage.ref(fileReference);

    const task: AngularFireUploadTask = this.afStorage
      .ref(fileReference)
      .put(blob, {
        contentType: 'image/png',
      });
    task.percentageChanges().subscribe((value) => {
      this.progressBarValue = Math.round(value!);
    });
    task
      .then((res) => {
        console.log(res);
        console.log(res.state);
        fileRef
          .getDownloadURL()
          .pipe(delay(0))
          .subscribe({
            next: (downloadURL) => {
              this.dataSrv.userCallingObj.img = downloadURL;
              console.log(this.dataSrv.userCallingObj);
              this.isLoadingPhotoPage = false;
              this.isUploadPhotoProcessOngoing = false;
              this.toastSrv.notify(
                'success',
                'Foto enviada com sucesso',
                '',
                3000
              );
                this.dataSrv.sendNotification();
             //this.routeNav.navigateByUrl('/callPage');
            },
            error: (err) => {
              console.log(err);
              this.toastSrv.notify(
                'error',
                'Erro ao efetuar chamada',
                'Houve um erro ao tentar subir a imagem. Tente novamente',
                3000
              );
              this.isLoadingPhotoPage = true;
              this.isUploadPhotoProcessOngoing = false;
            },
          });
      })
      .catch((err) => {
        console.log(err);
        this.toastSrv.notify(
          'error',
          'Erro ao efetuar chamada',
          'Houve um erro ao tentar subir a imagem. Tente novamente',
          3000
        );
        this.isLoadingPhotoPage = true;
        this.isUploadPhotoProcessOngoing = false;
      });
  }

  isFormValid(form: NgForm) {
    //console.log(form);

    let retorno = false;
    if (form.valid) {
      if (!this.isLoadingPhotoPage) {
        retorno = true;
      }
    }

    return retorno;
  }
}
