import { config } from './../../services/config';
import { Router } from '@angular/router';
import { ToastService } from './../../services/toast.service';
import { takeWhile, timer } from 'rxjs';
import { AgoraService } from './../../services/agora.service';
import { DataService } from './../../services/data.service';
import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { environment } from 'src/environments/environment';
import AgoraRTC, {
  IAgoraRTCClient,
  IMicrophoneAudioTrack,
  IRemoteAudioTrack,
} from 'agora-rtc-sdk-ng';
import { AngularFireFunctions } from '@angular/fire/compat/functions';

@Component({
  templateUrl: './call-page.component.html',
  styleUrls: ['./call-page.component.scss'],
})
export class CallPage implements OnInit, OnDestroy {
  pulseState = false;
  showCall = false;
  isCallMuted = false;
  hassCallStarted = false;
  AgoraOptions = {
    appId: environment.agora.appId,
    channel: 'd76-ea83-4980-8818-70b70012251c',
    token: '',
    uid: 0,
  };

  agoraChannelParameters = {
    localMicrophoneTrack: null as unknown as IMicrophoneAudioTrack,
    remoteAudioTrack: null as unknown as IRemoteAudioTrack,
    remoteUid: null as any,
  };
  agoraEngine!: IAgoraRTCClient;

  /*   timerForConection = timer(30000).pipe(
    takeWhile(() => {
      return !this.hassCallStarted;
    })
  ); */
  timerForMaxCallDuration = timer(180000);

  constructor(
    public dataSrv: DataService,
    private agoraSrv: AgoraService,
    private toastSrv: ToastService,
    private route: Router,
    private fns: AngularFireFunctions
  ) {}

  ngOnInit(): void {
    console.log(this.AgoraOptions);
    this.AgoraOptions.channel = sessionStorage.getItem(config.agoraChannelKey)!;
    console.log(this.AgoraOptions);
    this.startAgoraToken();
  }

  startAgoraToken() {
    const callable = this.fns.httpsCallable('agoraGenerateRTCToken');
    callable({
      channel: this.AgoraOptions.channel,
      isPublisher: true,
    }).subscribe((res) => {
      console.log(res);
      this.AgoraOptions.token = res.token;
      this.AgoraOptions.uid = res.uid;
      console.log(this.AgoraOptions);
      this.startAgoraEngine();
    });
  }
  startAgoraEngine() {
    this.agoraEngine = AgoraRTC.createClient({ mode: 'rtc', codec: 'vp8' });
    if(!environment.production) {
    //  this.toastSrv.notify('info', 'AgoraEngine carregada', '', 3000);
    }
    this.startRTCListener();
    this.join();
  }

  startRTCListener() {
    this.agoraEngine.on('user-joined', (user) => {
      this.toastSrv.notify(
        'success',
        'O proprietario entrou na chamada',
        'Seu uID é ' + user.uid,
        3000
      );
    });
    this.agoraEngine.on('user-published', async (user, mediaType) => {

      await this.agoraEngine.subscribe(user, mediaType);
      console.log('subscribe success');
      if (mediaType == 'audio') {
        this.agoraChannelParameters.remoteUid = Number(user.uid);
        this.agoraChannelParameters.remoteAudioTrack = user.audioTrack as any;
        this.agoraChannelParameters.remoteAudioTrack.play();
        this.hassCallStarted = true;
      }
      this.agoraEngine.on('user-unpublished', (user) => {
        this.agoraEngine.unsubscribe(user);
        console.log(user.uid + 'has left the channel');

       //
      });
      this.agoraEngine.on('user-left', (listener, reason) => {
        this.toastSrv.notify(
          'success',
          'Alguem saiu do canal',
          'Seu uID é ' + user.uid,
          3000
        );
        console.log(listener);
        console.log(reason);
        this.hangUpCall();
      })
    });
  }

  join() {
    this.agoraEngine
      .join(
        this.AgoraOptions.appId,
        this.AgoraOptions.channel,
        this.AgoraOptions.token,
        this.AgoraOptions.uid
      )
      .then(async (res) => {
        this.toastSrv.notify(
          'success',
          'Você entrou no canal',
          'Publicando canal para audio',
          3000
        );
        this.agoraChannelParameters.localMicrophoneTrack =
          await AgoraRTC.createMicrophoneAudioTrack();
         // debugger;
        this.agoraEngine
          .publish(this.agoraChannelParameters.localMicrophoneTrack)
          .then(() => {
          //  debugger;
            this.toastSrv.notify('success', 'Publish com sucesso!', '', 3000);
            this.timerForMaxCallDuration.subscribe(() => {
              this.toastSrv.notify(
                'info',
                'Tempo maximo de chamada excedido',
                'Chamada finalizada',
                5000
              );
              this.hangUpCall();
            });
          }).catch((err) => {
            console.log('Erro ao publicar som',err);

          });
      });
  }

  async muteCall() {
    /* console.clear() */
    console.log('L I N H A 142');
    console.log(this.agoraChannelParameters.localMicrophoneTrack.muted);
    if (this.agoraChannelParameters.localMicrophoneTrack.muted) {
      console.log('Ja tava mutado');
      this.isCallMuted = false;
      /* this.agoraEngine. */
      this.agoraChannelParameters.localMicrophoneTrack
        .setMuted(false)
        .then(() => {
          console.log('Fui desmutado');
        })
        .catch((err) => {
          console.log('Erro ao tentar desmutar');
          console.log(err);
        });
    } else {
      console.log('Não tava mutado');
      await this.agoraChannelParameters.localMicrophoneTrack
        .setMuted(true)
        .then(() => {
          console.log('Fui mutado');
          this.isCallMuted = true;
        })
        .catch((err) => {
          console.log('Erro ao tentar mutar');
          console.log(err);
        });
    }
  }
  hangUpCall() {
    this.agoraChannelParameters.localMicrophoneTrack.close();
    this.agoraEngine.leave().then(() => {
      this.dataSrv.cancelCallUser(false);
      this.toastSrv.notify('success', 'Você saiu do canal', '', 2000);
      this.route.navigateByUrl('home/' + this.agoraSrv.channelName);
      setTimeout(() => {
        this.dataSrv.resetApp();
      }, 3000);
    });
  }

  ngOnDestroy(): void {}
}



@Component({
  selector: 'darkmode-toggler',
  template: `<div>
    <button>Toggle dark mode</button>
    <p>{{isItActivated}}</p>
  </div>`,
  styles: []
})
export class DarkModeToggler {

  isItActivated = false
  /*  'disabled' |  'active' = 'disabled'; */
  @Output() stateChanged = new EventEmitter<string>();

  setDarkMode() {
this.isItActivated = !this.isItActivated;
const currentState = this.isItActivated ? 'active': 'disabled'
this.stateChanged.emit(currentState)

  }

}

@Component({
  selector: 'app-root',
  template: `<div>
    <darkmode-toggler (stateChanged)="handleStateChange($event)"></darkmode-toggler>
  </div>`,
  styles: []
})
export class Resource {
  handleStateChange(event) {
    console.log("Event value: " + event);
  }
}
