import { config } from './../../services/config';
import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { ToastService } from 'src/app/services/toast.service';
import { Geolocation } from '@capacitor/geolocation';
@Component({
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.scss'],
})
export class LandingPage implements OnInit {
  isGpsDataAvailable = false;
  gettingGPSLocation = false;
  constructor(
    public dataSrv: DataService,
    private route: ActivatedRoute,
    private routeNav: Router,
    private afStorage: AngularFireStorage,
    private toastSrv: ToastService
  ) {}

  ngOnInit(): void {
    this.dataSrv.findUser(this.route.snapshot.paramMap.get('tokenChamada')!);
    sessionStorage.setItem(
      config.agoraChannelKey,
      this.route.snapshot.paramMap.get('tokenChamada')!
    );
  }

  askGeoPermision() {
    this.gettingGPSLocation = true;
    Geolocation.checkPermissions()
      .then((res) => {
        console.log('12345', res);
        this.dataSrv.updateGeoLocation();
        setTimeout(() => {
          this.gettingGPSLocation = false;
          this.routeNav.navigateByUrl('/home');
        }, 1500);
      })
      .catch((err) => {
        console.log(err);
        this.gettingGPSLocation = false;
      });
  }
}
