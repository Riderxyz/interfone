import { ToastService } from './../../services/toast.service';
import { DataService } from 'src/app/services/data.service';
import { UsuarioInterface } from './../../models/usuario.interface';
import { Component, OnInit } from '@angular/core';
import {
  Camera,
  CameraDirection,
  CameraResultType,
  CameraSource,
  Photo,
} from '@capacitor/camera';
import { AngularFireStorageReference, AngularFireStorage, AngularFireUploadTask } from '@angular/fire/compat/storage';
import { NgForm } from '@angular/forms';
import { delay } from 'rxjs';
@Component({
  templateUrl: './photo.page.html',
  styleUrls: ['./photo.page.scss']
})
export class PhotoPage implements OnInit {
  imgToShow = '';
  proprietarioOBj!: UsuarioInterface;
  isGpsDataAvailable = false;
  isLoadingPhotoPage = false;
  isUploadPhotoProcessOngoing = false;
  progressBarStyle = {
    height: '30px',
    width: '30vh',
  };
  progressBarValue = 0;
  constructor(
    public dataSrv: DataService,
    private afStorage: AngularFireStorage,
    private toastSrv: ToastService
  ) { }

  ngOnInit(): void {
    this.onPhotoCall().finally(() => {

    });
  }

  async onPhotoCall() {
    this.isLoadingPhotoPage = true;
    const image = await Camera.getPhoto({
      quality: 50,
      // allowEditing: true,
      resultType: CameraResultType.DataUrl,
      allowEditing: false,
      width: 100,
      height: 100,
      direction: CameraDirection.Front,
      source: CameraSource.Camera,
    });
    console.log(image);
    this.imgToShow = image.dataUrl as any;
    console.log(this.imgToShow);
    // 'data:image/png;base64,' +  image.base64String;
    //this.isLoadingPhotoPage = false;
    // await this.uploadImageToCall(image);
  }

  async uploadImageToCall(image: Photo) {
    console.clear();
    this.isUploadPhotoProcessOngoing = true;
    const response = await fetch(image.dataUrl as any);
    const fileName = new Date().getTime();
    const blob = await response.blob();
    const fileReference =
      '/usuario/' +
      this.dataSrv.userCallingObj.idToCallTo +
      '/temp/' +
      fileName +
      '.png';
    const fileRef: AngularFireStorageReference =
      this.afStorage.ref(fileReference);

    const task: AngularFireUploadTask = this.afStorage
      .ref(fileReference)
      .put(blob, {
        contentType: 'image/png',
      });
    task.percentageChanges().subscribe((value) => {
      this.progressBarValue = Math.round(value!);
    });
    task
      .then((res) => {
        console.log(res);
        console.log(res.state);
        fileRef
          .getDownloadURL()
          .pipe(delay(0))
          .subscribe({
            next: (downloadURL) => {
              this.dataSrv.userCallingObj.img = downloadURL;
              console.log(this.dataSrv.userCallingObj);
              this.isLoadingPhotoPage = false;
              this.isUploadPhotoProcessOngoing = false;
            },
            error: (err) => {
              console.log(err);
              this.toastSrv.notify(
                'error',
                'Erro ao efetuar chamada',
                'Houve um erro ao tentar subir a imagem. Tente novamente',
                3000
              );
              this.isLoadingPhotoPage = true;
              this.isUploadPhotoProcessOngoing = false;
            },
          });
      })
      .catch((err) => {
        console.log(err);
        this.toastSrv.notify(
          'error',
          'Erro ao efetuar chamada',
          'Houve um erro ao tentar subir a imagem. Tente novamente',
          3000
        );
        this.isLoadingPhotoPage = true;
        this.isUploadPhotoProcessOngoing = false;
      });
  }


  sendDataTOCall() {
    console.log('Atiivando chamada');
    this.dataSrv.sendNotification();
  }
}
