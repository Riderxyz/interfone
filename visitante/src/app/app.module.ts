
import { environment } from './../environments/environment';
import { PrimeNGModule } from './primeNg.module';
import { HttpClientModule } from '@angular/common/http';
import { CdkModule } from './cdk.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule  } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { AngularFireMessagingModule } from '@angular/fire/compat/messaging';
import { AngularFireFunctionsModule } from '@angular/fire/compat/functions';
import { AngularFireAnalyticsModule, ScreenTrackingService, UserTrackingService } from '@angular/fire/compat/analytics';
/* import { initializeApp,provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../environments/environment';
import { provideAnalytics,getAnalytics,ScreenTrackingService,UserTrackingService } from '@angular/fire/analytics';
import { provideAuth,getAuth } from '@angular/fire/auth';
import { provideFirestore,getFirestore } from '@angular/fire/firestore';
import { provideFunctions,getFunctions } from '@angular/fire/functions';
import { provideMessaging,getMessaging } from '@angular/fire/messaging';
import { provideStorage,getStorage } from '@angular/fire/storage'; */

import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { HomePage } from './pages/home/home.component';
import { CallPage } from './pages/call-page/call-page.component';
import { PhotoPage } from './pages/photo/photo.page';
import { LandingPage } from './pages/landing/landing.page';


import { ToastComponent } from './components/toast/toast.component';

const Forms = [ReactiveFormsModule, FormsModule];
const AngularFire = [
  AngularFireModule.initializeApp(environment.firebase),
  AngularFirestoreModule,
  AngularFireStorageModule,
  AngularFireMessagingModule,
AngularFireFunctionsModule,
  AngularFireAnalyticsModule,
]
@NgModule({
  declarations: [AppComponent, HomePage, CallPage, ToastComponent, LandingPage, PhotoPage],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    CdkModule,
    PrimeNGModule,
    ...Forms,
    ...AngularFire,
    /*     provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideAnalytics(() => getAnalytics()),
    provideAuth(() => getAuth()),
    provideFirestore(() => getFirestore()),
    provideFunctions(() => getFunctions()),
    provideMessaging(() => getMessaging()),
    provideStorage(() => getStorage()) */
  ],
  providers: [
    ScreenTrackingService,UserTrackingService
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
