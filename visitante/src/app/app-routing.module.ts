import { PhotoPage } from './pages/photo/photo.page';
import { LandingPage } from './pages/landing/landing.page';
import { CallPage } from './pages/call-page/call-page.component';
import { HomePage } from './pages/home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

/*   {
    path: 'landing/:tokenChamada',
    component: LandingPage,
  }, */
  {
    path: 'home/:tokenChamada',
    component: HomePage,
  },
  {
    path: 'photo',
    component: PhotoPage,
  },
  {
    path: 'callPage',
    component: CallPage,
  },
  {
    path: '',
    redirectTo: 'home/e112aba2-ea00-4991-a322-5d14c64975b2',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
