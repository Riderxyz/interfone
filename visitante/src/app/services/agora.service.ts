import { ToastService } from './toast.service';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import AgoraRTC, {
  IAgoraRTCClient,
  IMicrophoneAudioTrack,
  IRemoteAudioTrack,
} from 'agora-rtc-sdk-ng';
import { Observable, Subject, timer, takeWhile } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AgoraService {
  uid = 0;
  private agoraEngine!: IAgoraRTCClient;
  channelName = '';
  deviceArr: MediaDeviceInfo[] = [];
  channelParams = {
    localAudio: null! as IMicrophoneAudioTrack,
    remoteAudio: null! as IRemoteAudioTrack,
    proprietarioUserId: null as any,
  };

  selectedOutput!: MediaDeviceInfo;
  proprietarioConectouSub = new Subject<boolean>();
  tempToken = '';
  isCallMuted = false;
  isCallStarted: boolean = false;
  timerForConection = timer(30000).pipe(
    takeWhile(() => {
      return !this.isCallStarted;
    })
  );
  timerForMaxCallDuration = timer(180000);

  constructor(
    private http: HttpClient,
    private toastSrv: ToastService,
    private fns: AngularFireFunctions //private db: AngularFirestore
  ) {
  }

  private assignClientHandlers(): void {
    this.agoraEngine.on('user-published', (remoteUser, mediaType) => {
      this.proprietarioConectouSub.next(true);
      if (mediaType === 'audio') {
        this.channelParams.localAudio.play();
        this.channelParams.remoteAudio = remoteUser.audioTrack!;
        this.channelParams.remoteAudio.play();
        this.toastSrv.notify(
          'success',
          'Proprietario Atendeu a chamada',
          '',
          3000
        );
      }
      this.agoraEngine.on('user-unpublished', (user) => {
        this.toastSrv.notify(
          'info',
          'O proprietario saiu da chamada',
          '',
          5000
        );
      });

      this.agoraEngine.on('token-privilege-did-expire', () => {
        const callable = this.fns.httpsCallable('agoraGenerateRTCToken');
        callable({
          channel: this.channelName,
          isPublisher: true,
        }).subscribe((res) => {
          console.log(res);
          this.agoraEngine.renewToken(res.token);
        })

      });
    });
  }
}
