import { IAgoraRTCClient } from 'agora-rtc-sdk-ng';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { AgoraService } from './agora.service';
import { ToastService } from './toast.service';
import { ChamadaInterface } from './../models/chamada.interface';
import { HttpClient } from '@angular/common/http';
import { UsuarioInterface } from './../models/usuario.interface';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { VisitanteInterface } from './../models/visitante.interface';
import { Injectable } from '@angular/core';
import { Route, Router } from '@angular/router';
import * as uuid from 'uuid-random';
import { Geolocation } from '@capacitor/geolocation';
@Injectable({
  providedIn: 'root',
})
export class DataService {
  private _userCallingObj!: VisitanteInterface;
  private _runTimeCallName = uuid();
  private pushTokenArr: string[] = [];
  constructor(
    private db: AngularFirestore,
    private http: HttpClient,
    private toastSrv: ToastService,
    //private agoraSrv: AgoraService,
    private route: Router,
    //private fns: AngularFireFunctions
  ) {
    this.userCallingObj = {
      nome: '',
      idToCallTo: '',
      img: null as any,
      userIdQueAtendeu: null as any,
      data: null as any,
      localizacao: {
        long: null as any,
        lat: null as any,
      },
      tempoChamada: 0,
    };
  }
  get userCallingObj(): VisitanteInterface {
    return this._userCallingObj;
  }
  set userCallingObj(_userCallingObj: VisitanteInterface) {
    this._userCallingObj = _userCallingObj;
  }

  findUser(qrCodeId: string) {
    const user = this.db.collection('/usuarios', (res) =>
      res.where('tokenChamada', '==', qrCodeId)
    );
    user.valueChanges().subscribe((resWith: any[]) => {
      let res: UsuarioInterface[] = [];
      res = resWith;
      console.log('pesquisa de codigos qR', res);
      console.log(res[0]);
      this.userCallingObj.idToCallTo = res[0].id;
      this.preloadNotificationToken(res[0])
    });
  }

  preloadNotificationToken(userData: UsuarioInterface) {
    this.db.collection('/usuarios/').doc(userData.id)
    .collection("notificationToken").valueChanges().subscribe((pushTokens) => {
      this.pushTokenArr = [];
      pushTokens.forEach((el, index) => {
       this.pushTokenArr.push(el['token'])
      })
  })
}

  callUser() {
    let novaChamada: ChamadaInterface = {
      data: new Date().getTime(),
      img: this.userCallingObj.img,
      isChamadaFinalizada: false,
      nome: this.userCallingObj.nome,
      tempoChamada: 0,
      localizacaoLat: this.userCallingObj.localizacao.lat,
      localizacaoLong: this.userCallingObj.localizacao.long,
    };
      this.db
        .collection('/usuarios/')
        .doc(this.userCallingObj.idToCallTo)
        .collection('chamadaAtiva')
        .doc('chamadaAtiva')
        .set(novaChamada)
        .then((res) => {
          console.log('Chamada de push feita com sucesso');
          this.route.navigateByUrl('/callPage');
          // this.navCtrl.navigateForward('callPage');
        });
  }

  sendNotification() {
    const randomID = Math.floor(Math.random() * 500000) + 1;
    console.log(this.userCallingObj);

    let bodyForPost = {
      userId: this.userCallingObj.idToCallTo,
      pushTokenArr: this.pushTokenArr,
      data: {
        title:
          'Você esta recebendo uma ligação de ' + this.userCallingObj.nome,
        body: 'Para aceitar, clique aqui',
        action: 'Incoming call',
        proprietarioId: this.userCallingObj.idToCallTo,
        id: randomID.toString(),
        visitorsPhoto: this.userCallingObj.img,
        nome: this.userCallingObj.nome,
        clickAction: 'FLUTTER_NOTIFICATION_CLICK',
        channelId: 'ligacao',
      },
      visitante: this.userCallingObj.nome,
    }
    console.log(JSON.stringify(bodyForPost));

    this.http.post('https://us-central1-interfone-a2139.cloudfunctions.net/LigacaoNotification',
      bodyForPost
    ).subscribe({
      next: (res) => {
        console.log(res);
        this.callUser();
      },
      error: (err) => {
        console.log(err);
        this.toastSrv.notify(
          'error',
          'Erro ao efetuar chamada',
          'Houve um erro ao tentar chamar o proprietario. Tente novamente',
          3000
        );
      },
    });
  }

  cancelCallUser(chamadaCaiuDevidoAoTimeOut: boolean) {
    this.db
      .collection('/usuarios/')
      .doc(this.userCallingObj.idToCallTo)
      .collection('chamadaAtiva')
      .doc('chamadaAtiva')
      .update({
        isChamadaFinalizada: true,
      })
      .finally(async () => {
        if (chamadaCaiuDevidoAoTimeOut) {
          this.toastSrv.notify(
            'info',
            'A pessoa que é dona do local não esta atendendo',
            'Tente novamente ou volte mais tarde.',
            4000
          );
        } else{
          this.toastSrv.notify('info', 'Chamada finalizada', 'Limpando app para nova chamada', 5000)
        }
      });
  }

  resetApp() {
    window.location.href = window.location.href.replace(/#.*$/, '');
  }

  updateGeoLocation() {
    Geolocation.watchPosition(
      {
        enableHighAccuracy: false,
        timeout: 2000,
        maximumAge: 60000,
      },
      (coordinates) => {
        console.log('mudança de GPS', coordinates);
        //this.isGpsDataAvailable = true;
        this.userCallingObj.localizacao.lat =
          coordinates!.coords.latitude;
        this.userCallingObj.localizacao.long =
          coordinates!.coords.longitude;
      }
    );
  }
}
