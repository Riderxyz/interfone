export interface ChamadaInterface {
  data: number;
  endTime?: number;
  startTime?: number;
  img: string;
  isChamadaFinalizada: boolean;
  nome: string;
  tempoChamada: number;
  localizacaoLat?: number;
  localizacaoLong?: number;
}
