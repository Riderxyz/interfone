export interface VisitanteInterface {

  nome: string;
  data: Date;
  idToCallTo: string;
  img: string;
  localizacao: {
    lat: number;
    long: number;
  };
  userIdQueAtendeu: string;
  tempoChamada: any;
}
