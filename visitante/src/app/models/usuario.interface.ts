import { ChamadaInterface } from './chamada.interface';
export interface UsuarioInterface {
  nome: string;
  id: string;
  email: string;
  photo?: string;
  chamadaAtual?: ChamadaInterface;
  historicoChamadas?: ChamadaInterface[];
}
